#!/usr/bin/env bash

echo "Proov project bash!"
echo "installing module..."

cd frontend
NPM install

echo "run build for production..."
NPM run build

cd ..
mvn clean install