# Spring mvc 4

This is a sample contacts crud angular project for anyone looking to get up and running with Spring mvc.
Using maven to compile typeScript and java files and the embedded database Hsql

## Pre - requisites
- You need to have [Maven 3](https://maven.apache.org/)
- You need to have [Java 8](https://www.java.com/en/)

## Features
- Spring Mvc 4.3.5
- Hsqldb 2.3
- Tomcat-7 2.2

## Run in dev mode
``` bash
# compile and start in development mode
 mvn clean tomcat7:run
        -- or --
# start webpack development server
 cd angular2
 npm start
```
