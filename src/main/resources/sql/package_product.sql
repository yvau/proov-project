﻿INSERT INTO public.package_product(id, count, name, price, price_tva, type)
  VALUES(1, '1', 'basic', '25.00', '0', 'for_rent');
INSERT INTO public.package_product(id, count, name, price, price_tva, type)
  VALUES(2, '3', 'medium', '60.00', '0', 'for_rent');
INSERT INTO public.package_product(id, count, name, price, price_tva, type)
  VALUES(3, '10', 'enterprise', '120.00', '0', 'for_rent');
INSERT INTO public.package_product(id, count, name, price, price_tva, type)
  VALUES(4, '3', 'basic', '99.00', '0', 'for_sale');
INSERT INTO public.package_product(id, count, name, price, price_tva, type)
  VALUES(5, '6', 'medium', '199.00', '0', 'for_sale');
INSERT INTO public.package_product(id, count, name, price, price_tva, type)
  VALUES(6, '10', 'enterprise', '399.00', '0', 'for_sale');
