﻿INSERT INTO country (id,"name",continent,tld,currency_code,currency_name,phone,postal_code,postal_format,languages) VALUES (
'CA','Canada','NA','.ca','CAD','Dollar','1','@#@ #@#','^([ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ]) ?(\d[ABCEGHJKLMNPRSTVWXYZ]\d)$ ','en-CA,fr-CA,iu');
INSERT INTO country (id,"name",continent,tld,currency_code,currency_name,phone,postal_code,postal_format,languages) VALUES (
'US','United States','NA','.us','USD','Dollar','1','#####-####','^\d{5}(-\d{4})?$','en-US,es-US,haw,fr');
