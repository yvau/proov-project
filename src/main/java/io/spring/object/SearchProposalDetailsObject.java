package io.spring.object;

import java.math.BigDecimal;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 9/30/2017
 */
public class SearchProposalDetailsObject {

    private Integer id;

    private BigDecimal priceMaximum;

    private BigDecimal priceMinimum;

    private String typeOfProperty;

    private String Location;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getPriceMaximum() {
        return priceMaximum;
    }

    public void setPriceMaximum(BigDecimal priceMaximum) {
        this.priceMaximum = priceMaximum;
    }

    public BigDecimal getPriceMinimum() {
        return priceMinimum;
    }

    public void setPriceMinimum(BigDecimal priceMinimum) {
        this.priceMinimum = priceMinimum;
    }

    public String getTypeOfProperty() {
        return typeOfProperty;
    }

    public void setTypeOfProperty(String typeOfProperty) {
        this.typeOfProperty = typeOfProperty;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }
}
