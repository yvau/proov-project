package io.spring.object;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 9/29/2017
 */
public class SearchProposalObject {

    private String categoryOfProperty;

    private String isPreApproved;

    private String isFirstBuyer;

    private String isUrgent;

    private BigDecimal priceMinimum;

    private BigDecimal priceMaximum;

    //bi-directional many-to-one association to LocationHasFileOfNeedSellerLessorPropertyDetail
    private List<SearchProposalDetailsObject> searchProposalDetails;

    public String getCategoryOfProperty() {
        return categoryOfProperty;
    }

    public void setCategoryOfProperty(String categoryOfProperty) {
        this.categoryOfProperty = categoryOfProperty;
    }

    public String getIsPreApproved() {
        return isPreApproved;
    }

    public void setIsPreApproved(String isPreApproved) {
        this.isPreApproved = isPreApproved;
    }

    public String getIsFirstBuyer() {
        return isFirstBuyer;
    }

    public void setIsFirstBuyer(String isFirstBuyer) {
        this.isFirstBuyer = isFirstBuyer;
    }

    public String getIsUrgent() {
        return isUrgent;
    }

    public void setIsUrgent(String isUrgent) {
        this.isUrgent = isUrgent;
    }

    public BigDecimal getPriceMinimum() {
        return priceMinimum;
    }

    public void setPriceMinimum(BigDecimal priceMinimum) {
        this.priceMinimum = priceMinimum;
    }

    public BigDecimal getPriceMaximum() {
        return priceMaximum;
    }

    public void setPriceMaximum(BigDecimal priceMaximum) {
        this.priceMaximum = priceMaximum;
    }

    public List<SearchProposalDetailsObject> getSearchProposalDetails() {
        return searchProposalDetails;
    }

    public void setSearchProposalDetails(List<SearchProposalDetailsObject> searchProposalDetails) {
        this.searchProposalDetails = searchProposalDetails;
    }
}
