package io.spring.object;

/**
 * @author Yvau
 * @version 1.0
 * @date 4/6/2018
 */
public class BookmarkObject {

    private String nature;

    private long property;

    private long proposal;

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public long getProperty() {
        return property;
    }

    public void setProperty(long property) {
        this.property = property;
    }

    public long getProposal() {
        return proposal;
    }

    public void setProposal(long proposal) {
        this.proposal = proposal;
    }
}
