package io.spring.object;

import java.math.BigDecimal;

/**
 * Specifies class ProposalObject used to handle methods related to
 * <code>Proposal Object</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/21/2017
 */
public class ProposalObject {

    private Long id;

    private String location;

    private String status;

    private Boolean isUrgent;

    private String ageOfProperty;

    private String typeOfProperty;

    private Boolean isFurnished;

    private Integer bathrooms;

    private Integer bedrooms;

    private BigDecimal priceMinimum;

    private BigDecimal priceMaximum;

    private BigDecimal size;

    private String typeOfProposal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getUrgent() {
        return isUrgent;
    }

    public void setUrgent(Boolean urgent) {
        isUrgent = urgent;
    }

    public String getAgeOfProperty() {
        return ageOfProperty;
    }

    public void setAgeOfProperty(String ageOfProperty) {
        this.ageOfProperty = ageOfProperty;
    }

    public String getTypeOfProperty() {
        return typeOfProperty;
    }

    public void setTypeOfProperty(String typeOfProperty) {
        this.typeOfProperty = typeOfProperty;
    }

    public Boolean getIsFurnished() {
        return isFurnished;
    }

    public void setIsFurnished(Boolean isFurnished) {
        this.isFurnished = isFurnished;
    }

    public Integer getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(Integer bathrooms) {
        this.bathrooms = bathrooms;
    }

    public Integer getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(Integer bedrooms) {
        this.bedrooms = bedrooms;
    }

    public BigDecimal getPriceMinimum() {
        return priceMinimum;
    }

    public void setPriceMinimum(BigDecimal priceMinimum) {
        this.priceMinimum = priceMinimum;
    }

    public BigDecimal getPriceMaximum() {
        return priceMaximum;
    }

    public void setPriceMaximum(BigDecimal priceMaximum) {
        this.priceMaximum = priceMaximum;
    }

    public BigDecimal getSize() {
        return size;
    }

    public void setSize(BigDecimal size) {
        this.size = size;
    }

    public String getTypeOfProposal() {
        return typeOfProposal;
    }

    public void setTypeOfProposal(String typeOfProposal) {
        this.typeOfProposal = typeOfProposal;
    }
}
