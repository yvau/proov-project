package io.spring.object;

/**
 * @author Yvau
 * @version 1.0
 * @date 2/7/2018
 */
public class FeedActivityObject {

    private Long id;

    private String isInteresting;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIsInteresting() {
        return isInteresting;
    }

    public void setIsInteresting(String isInteresting) {
        this.isInteresting = isInteresting;
    }
}
