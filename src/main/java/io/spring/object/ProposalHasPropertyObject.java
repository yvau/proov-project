package io.spring.object;

public class ProposalHasPropertyObject {

	private Long property;

	private Long proposal;

	private Long payment;

	public ProposalHasPropertyObject() {
	}

	public Long getProperty() {
		return property;
	}

	public void setProperty(Long property) {
		this.property = property;
	}

	public Long getProposal() {
		return proposal;
	}

	public void setProposal(Long proposal) {
		this.proposal = proposal;
	}

	public Long getPayment() {
		return payment;
	}

	public void setPayment(Long payment) {
		this.payment = payment;
	}
}