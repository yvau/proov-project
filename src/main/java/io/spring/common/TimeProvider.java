package io.spring.common;

import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
/**
 * @author Yvau
 * @version 1.0
 * @date 12/14/2017
 */
@Component
public class TimeProvider implements Serializable {

    private static final long serialVersionUID = -3301695478208950415L;

    public Date now() {
        return new Date();
    }
}
