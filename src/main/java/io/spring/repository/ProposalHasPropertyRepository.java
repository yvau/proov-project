package io.spring.repository;

import io.spring.model.ProposalHasProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * @author Yvau
 * @version 1.0
 * @date 2/9/2018
 */
public interface ProposalHasPropertyRepository  extends JpaRepository<ProposalHasProperty, Long> , QueryDslPredicateExecutor<ProposalHasProperty> {

}
