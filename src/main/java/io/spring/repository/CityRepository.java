package io.spring.repository;

import io.spring.model.City;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public interface CityRepository extends JpaRepository<City, String> {

    List<City> findFirst300ByNameAsciiContainingIgnoreCase(String name);

}
