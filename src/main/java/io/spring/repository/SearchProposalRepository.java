package io.spring.repository;

import io.spring.model.SearchProposal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * @author Yvau
 * @version 1.0
 * @date 12/11/2017
 */
public interface SearchProposalRepository extends JpaRepository<SearchProposal, Long>, QueryDslPredicateExecutor<SearchProposal> {

}
