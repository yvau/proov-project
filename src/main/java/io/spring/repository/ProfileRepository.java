package io.spring.repository;

import io.spring.model.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import java.util.Optional;

/**
 * Specifies class ProfileRepository used to handle methods related to
 * <code>{@link JpaRepository}</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public interface ProfileRepository extends JpaRepository<Profile, Long>, QueryDslPredicateExecutor<Profile> {

    Optional<Profile> findByCredential(String credential);

    Optional<Profile> findByToken(String token);

}