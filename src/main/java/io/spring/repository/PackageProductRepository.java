package io.spring.repository;

import io.spring.model.PackageProduct;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Yvau
 * @version 1.0
 * @date 2/14/2018
 */
public interface PackageProductRepository extends JpaRepository<PackageProduct, Integer> {
}
