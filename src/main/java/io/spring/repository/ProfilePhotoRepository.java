package io.spring.repository;

import io.spring.model.ProfilePhoto;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Specifies class PropertyPhotoRepository used to handle methods related to
 * <code>{@link JpaRepository}</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface ProfilePhotoRepository extends JpaRepository<ProfilePhoto, Long> {

}
