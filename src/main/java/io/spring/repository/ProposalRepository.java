package io.spring.repository;

import io.spring.model.Proposal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Specifies class ProposalRepository used to handle methods related to
 * <code>{@link JpaRepository}</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public interface ProposalRepository extends JpaRepository<Proposal, Long>, QueryDslPredicateExecutor<Proposal> {

    Proposal findByIdAndTypeOfProposal(Long id, String typeOfProposal);
}
