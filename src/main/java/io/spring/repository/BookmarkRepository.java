package io.spring.repository;

import io.spring.model.Bookmark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

/**
 * Specifies class AutoIncrementRepository used to handle methods related to
 * <code>{@link JpaRepository}</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface BookmarkRepository extends JpaRepository<Bookmark, Long>, QueryDslPredicateExecutor<Bookmark> {

}
