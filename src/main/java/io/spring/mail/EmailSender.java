package io.spring.mail;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/7/2017
 */
public interface EmailSender {

    void send(String recipient, String subject, String body);
}
