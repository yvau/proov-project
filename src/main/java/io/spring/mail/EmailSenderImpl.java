package io.spring.mail;

import io.spring.config.ApplicationProfiles;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Map;
import java.util.Properties;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/30/2017
 */
@Component
@PropertySource(value = { "classpath:application-${profile}.properties" })
public class EmailSenderImpl implements EmailSender {

    @Resource(name = "emailConfiguration")
    private Map<String, String> emailConfiguration;

    String MAIL_PROTOCOL="mail.transport.protocol";
    String MAIL_HOST="mail.smtp.host";
    String MAIL_AUTH="mail.smtp.auth";
    String MAIL_TLS="mail.smtp.starttls.enable";
    String MAIL_PORT="mail.smtp.port";
    String MAIL_DEBUG="mail.debug";
    String MAIL_USERNAME="mail.username";
    String MAIL_PASSWORD="mail.password";

    @Resource
    Environment env;

    @Async
    public void send(String recipient, String subject, String body) {
        Session session = Session.getInstance(getProperties(), new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
              return new PasswordAuthentication(emailConfiguration.get(MAIL_USERNAME), emailConfiguration.get(MAIL_PASSWORD));
            }
        });
        try {
            Message message = createMessage(session, recipient, subject, body);
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private Message createMessage(Session session, String recipient, String subject, String body) throws MessagingException {
        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("mobiot.fabrice@gmail.com"));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipient));
        message.setSubject(subject);
        message.setText(body, "utf-8", "html");
        return message;
    }

    private Properties getProperties() {
        Properties props = new Properties();
        props.put(MAIL_HOST, emailConfiguration.get(MAIL_HOST));
        props.put(MAIL_AUTH, emailConfiguration.get(MAIL_AUTH));
        props.put(MAIL_TLS, emailConfiguration.get(MAIL_TLS));
        props.put(MAIL_PORT, emailConfiguration.get(MAIL_PORT));
        props.put(MAIL_DEBUG, emailConfiguration.get(MAIL_DEBUG));
        if (env.getActiveProfiles().equals(ApplicationProfiles.PRODUCTION)) {
            // props.put(MAIL_USERNAME, emailConfiguration.get(MAIL_USERNAME));
            // props.put(MAIL_PASSWORD, emailConfiguration.get(MAIL_PASSWORD));
            props.put(MAIL_PROTOCOL,  emailConfiguration.get(MAIL_PROTOCOL));
        }
        return props;
    }
}
