package io.spring.exceptionHandler;

import org.springframework.security.core.AuthenticationException;

/**
 * The <code> LibraryAuthenticationErrorException </ code> class is an exception that
 * indicates errors in the authentication process. It is generic and it is the exceptions
 * daughters specify the error they represent.
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public class IoSpringAuthenticationErrorException extends AuthenticationException {
	
	private static final long serialVersionUID = 1L;

	public IoSpringAuthenticationErrorException(String msg) {
		super(msg);
	}

	public IoSpringAuthenticationErrorException(String msg, Throwable t) {
		super(msg, t);
	}
}
