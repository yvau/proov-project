package io.spring.exceptionHandler;

import org.springframework.security.core.AuthenticationException;
/**
 * @author Yvau
 * @version 1.0
 * @date 12/15/2017
 */
public class JwtTokenMissingException extends AuthenticationException {


    public JwtTokenMissingException(String msg) {
        super(msg);
    }
}
