package io.spring.exceptionHandler;

/**
 * Specifies class InvalidCredentialsException used to handle methods related to
 * <code>AuthenticationErrorException</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public class InvalidCredentialsException extends IoSpringAuthenticationErrorException {

	private static final long serialVersionUID = 1L;
	
	public InvalidCredentialsException(String msg) {
		super(msg);
	}

}
