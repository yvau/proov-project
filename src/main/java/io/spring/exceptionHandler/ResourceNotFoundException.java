package io.spring.exceptionHandler;


/**
 * @author Xiaoyue Xiao
 */
public class ResourceNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -2565431806475335331L;

    private String resourceName;
    private String id;

    @Override
    public String getMessage() {
        return org.springframework.util.StringUtils.capitalize(resourceName) + " with id " + id + " is not found.";
    }

    public String getId() {
        return id;
    }

    public ResourceNotFoundException setId(String id) {
        this.id = id;
        return this;
    }

    public String getResourceName() {
        return resourceName;
    }

    public ResourceNotFoundException setResourceName(String resourceName) {
        this.resourceName = resourceName;
        return this;
    }
}
