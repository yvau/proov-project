package io.spring.constant;

/**
 * Specifies class used to gather multiples variables constant related to
 * <code>Numeric constant</code>
 * (NULL_VALUE, TEN_VALUE...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public class Numeric {

    public static final int NULL_VALUE = 0; // Default number value
    public static final int TEN_VALUE = 10; // Default size of per page
    public static final int MINIMUMLENGTH = 6;

    /**
     * Prevent instantiation
     */
    private Numeric() {
    }

}
