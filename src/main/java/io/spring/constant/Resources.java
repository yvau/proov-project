package io.spring.constant;

/**
 * Specifies class used to gather multiples variables constant related to
 * <code>Entities</code>
 * (PROFILE, LOCATION, PROPOSAL...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public class Resources {

    public static final String PROFILE = "profile";
    public static final String PROFILE_INFORMATION = "profile_information";
    public static final String LOCATION = "location";
    public static final String PROPERTY = "property";
    public static final String PROPOSAL = "proposal";
    public static final String FEED = "feed";
    public static final String PAYMENT_DETAIL = "payment_detail";
    public static final String SEARCH_PROPOSAL = "search_proposal";
    public static final String PROPERTY_PHOTO = "property_photo";
    // public static final String PROFILE_PHOTO = "profile_photo";
    public static final String BOOKMARK = "bookmark";
    public static final String PROPOSAL_HAS_PROPERTY = "proposal_has_property";

    /**
     * Prevent instantiation
     */
    private Resources() {
    }

}
