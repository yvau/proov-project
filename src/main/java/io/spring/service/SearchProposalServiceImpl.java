package io.spring.service;

import com.querydsl.core.BooleanBuilder;
import io.spring.model.Proposal;
import io.spring.model.QSearchProposal;
import io.spring.model.SearchProposal;
import io.spring.repository.SearchProposalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

/**
 * @author Yvau
 * @version 1.0
 * @date 12/11/2017
 */
@Service
public class SearchProposalServiceImpl implements SearchProposalService  {

    private final SearchProposalRepository repository;

    private final AppService appService;

    // private final FeedService feedService;

    private final FeedActivityService feedActivityService;

    @Autowired
    public SearchProposalServiceImpl(final SearchProposalRepository repository,
                                     final AppService appService,
                                     // final FeedService feedService,
                                     final FeedActivityService feedActivityService) {
        this.repository = repository;
        this.appService = appService;
        // this.feedService = feedService;
        this.feedActivityService = feedActivityService;
    }

    /**
     * save <code>{@link SearchProposal}</code>
     * @param searchProposal
     */
    @Override
    public void save(@Param("searchProposal") SearchProposal searchProposal) {
        //
        repository.saveAndFlush(searchProposal);
        return;
    }

    private Iterable<SearchProposal> findAllByProposal(Proposal proposal) {
        //
        return repository.findAll(where(proposal));
    }

    public void asyncSchedule(Proposal proposal) {
        /*findAllByProposal(proposal).forEach(searchProposal -> {
            Feed feed = new Feed();
            feed.setId(appService.getId(Resources.FEED));
            feed.setDateOfCreation(Timestamp.from(Variables.timestamp));
            feed.setProfile(searchProposal.getProfile());
            feed.setType(FeedType.feed.toString());
            feed.setProposal(proposal);
            feedService.save(feed);

            ProfileActivity profileActivity = new ProfileActivity();
            profileActivity.setId(appService.getId(Resources.PROFILE_ACTIVITY));
            profileActivity.setDateOfCreation(Timestamp.from(Variables.timestamp));
            profileActivity.setIsRead(false);
            profileActivity.setProfile1(searchProposal.getProfile());
            profileActivity.setProfile2(proposal.getProfile());
            profileActivityService.save(profileActivity);

            ProfileActivityDetail profileActivityDetail = new ProfileActivityDetail();
            profileActivityDetail.setId(appService.getId(Resources.PROFILE_ACTIVITY_DETAIL));
            profileActivityDetail.setProposal(proposal);
            profileActivityDetail.setType("");
            profileActivityDetail.setDateOfCreation(Timestamp.from(Variables.timestamp));
            profileActivityDetail.setProfileActivity(profileActivity);
            profileActivityDetailService.save(profileActivityDetail);
        });*/
    }

    private BooleanBuilder where(Proposal Proposal) {
        QSearchProposal qSearchProposal =QSearchProposal.searchProposal;
        BooleanBuilder where = new BooleanBuilder();

        return where;
    }
}
