package io.spring.service;

import io.spring.model.PaymentDetail;

/**
 * Specifies class AutoIncrementService used to handle methods related to
 * <code>Entities</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface PaymentDetailService {

    void save(PaymentDetail paymentDetail);

}
