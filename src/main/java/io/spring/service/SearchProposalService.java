package io.spring.service;

import io.spring.model.Proposal;
import io.spring.model.SearchProposal;

/**
 * @author Yvau
 * @version 1.0
 * @date 12/11/2017
 */
public interface SearchProposalService {

    void save(SearchProposal searchProposal);

    void asyncSchedule(Proposal proposal);
}
