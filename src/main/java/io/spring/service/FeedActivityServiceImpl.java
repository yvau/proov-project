package io.spring.service;

import com.querydsl.core.BooleanBuilder;
import io.spring.model.FeedActivity;
import io.spring.repository.FeedActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Specifies class AutoIncrementService used to handle methods related to
 * <code>Entities</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service
public class FeedActivityServiceImpl implements FeedActivityService {

    private final FeedActivityRepository repository;

    @Autowired
    public FeedActivityServiceImpl(final FeedActivityRepository repository) {
        this.repository = repository;
    }

    /**
     *
     * @param feedActivity
     */
    @Override
    public void save(final FeedActivity feedActivity) {
        repository.save(feedActivity);
        return;
    }

    @Override
    public Page<FeedActivity> findAll(FeedActivity feedActivity, Pageable pageable) {
        //
        return repository.findAll(where(feedActivity), pageable);
    }

    private BooleanBuilder where(FeedActivity feedActivity) {
        // QFeedActivity qFeedActivity = QFeedActivity.feedActivity;
        BooleanBuilder where = new BooleanBuilder();

        return where;
    }


    @Override
    public Optional<FeedActivity> getById(Long id) {

        return Optional.ofNullable(repository.findOne(id));
    }

}
