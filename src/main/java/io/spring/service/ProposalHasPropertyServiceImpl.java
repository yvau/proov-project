package io.spring.service;

import com.querydsl.core.BooleanBuilder;
import io.spring.model.ProposalHasProperty;
import io.spring.model.QProposalHasProperty;
import io.spring.repository.ProposalHasPropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Objects;

/**
 * Specifies class AutoIncrementService used to handle methods related to
 * <code>Entities</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service
public class ProposalHasPropertyServiceImpl implements ProposalHasPropertyService {

    private final ProposalHasPropertyRepository repository;

    @Autowired
    public ProposalHasPropertyServiceImpl(final ProposalHasPropertyRepository repository) {
        this.repository = repository;
    }

    /**
     *
     * @param proposalHasProperty
     */
    @Override
    public void save(final ProposalHasProperty proposalHasProperty) {
        repository.save(proposalHasProperty);
        return;
    }

    /**
     *
     * @param proposalHasProperty
     * @param pageable
     * @return
     */
    @Override
    public Page<ProposalHasProperty> findAll(ProposalHasProperty proposalHasProperty, Pageable pageable) {
        return repository.findAll(where(proposalHasProperty), pageable);
    }

    private BooleanBuilder where(ProposalHasProperty proposalHasProperty) {
        QProposalHasProperty qProposalHasProperty = QProposalHasProperty.proposalHasProperty;
        BooleanBuilder where = new BooleanBuilder();
        if (!Objects.isNull(proposalHasProperty.getProposal()))
            where.and(qProposalHasProperty.proposal.id.eq(proposalHasProperty.getProposal().getId()));

        return where;
    }

}
