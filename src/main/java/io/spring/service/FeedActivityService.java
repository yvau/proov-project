package io.spring.service;

import io.spring.model.FeedActivity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Specifies class AutoIncrementService used to handle methods related to
 * <code>Entities</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface FeedActivityService {

    void save(FeedActivity feedActivity);

    Page<FeedActivity> findAll(FeedActivity feedActivity, Pageable pageable);

    Optional<FeedActivity> getById(final Long id);
}
