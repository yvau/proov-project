package io.spring.service;

import io.spring.model.ProfilePhoto;
import io.spring.repository.ProfilePhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Specifies class ProfileInformationService used to handle methods related to
 * <code>ProfileInformation Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/03/2017
 */
@Service
public class ProfilePhotoServiceImpl implements ProfilePhotoService {

	private final ProfilePhotoRepository repository;

	@Autowired
	public ProfilePhotoServiceImpl(final ProfilePhotoRepository repository) {
		this.repository = repository;
	}

	/**
	 *
	 * @param profilePhoto
	 */
	@Override
	public void save (ProfilePhoto profilePhoto) {
		repository.save(profilePhoto);
		return;
	}

	@Override
	public Optional<ProfilePhoto> getById(Long id) {
		return Optional.ofNullable(repository.findOne(id));
	}
}
