package io.spring.service;

import io.spring.model.Bookmark;

/**
 * Specifies class AutoIncrementService used to handle methods related to
 * <code>Entities</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface BookmarkService {

    void save(Bookmark bookmark);

    // Page<FeedActivity> findAll(FeedActivity feedActivity, Pageable pageable);

}
