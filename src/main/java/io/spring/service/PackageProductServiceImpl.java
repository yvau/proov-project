package io.spring.service;

import io.spring.model.PackageProduct;
import io.spring.repository.PackageProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Specifies class AutoIncrementService used to handle methods related to
 * <code>Entities</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service
public class PackageProductServiceImpl implements PackageProductService {

    private final PackageProductRepository repository;

    @Autowired
    public PackageProductServiceImpl(final PackageProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<PackageProduct> getById(Integer id) {

        return Optional.ofNullable(repository.findOne(id));
    }

}
