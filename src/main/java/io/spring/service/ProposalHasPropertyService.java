package io.spring.service;

import io.spring.model.ProposalHasProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Specifies class AutoIncrementService used to handle methods related to
 * <code>Entities</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
public interface ProposalHasPropertyService {

    void save(ProposalHasProperty proposalHasProperty);

    Page<ProposalHasProperty> findAll(ProposalHasProperty proposalHasProperty, Pageable pageable);
}
