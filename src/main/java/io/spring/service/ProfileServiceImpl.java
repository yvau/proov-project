package io.spring.service;

import com.querydsl.core.BooleanBuilder;
import io.spring.model.Profile;
import io.spring.model.QProfile;
import io.spring.repository.ProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Specifies class ProfileService used to handle methods related to
 * <code>Profile Entity</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service
public class  ProfileServiceImpl implements ProfileService {

	private final ProfileRepository repository;

	/**
	 *
	 * @param repository
	 */
	@Autowired
	public ProfileServiceImpl(final ProfileRepository repository) {
		this.repository = repository;
	}

	/**
	 *
	 * @param credential
	 * @return
	 */
	@Override
	public Optional<Profile> getByCredential(final String credential) {
		return repository.findByCredential(credential);
	}

	/**
	 *
	 * @param token
	 * @return
	 */
	@Override
	public Optional<Profile> getByToken(final String token) {
		return repository.findByToken(token);
	}

	/**
	 *
	 * @param id
	 * @return
	 */
	@Override
	public Optional<Profile> getById(final Long id) {
		return Optional.ofNullable(repository.findOne(id));
	}

	/**
	 *
	 * @param profile
	 * @param pageable
	 * @return
	 */
	@Override
	public Page<Profile> getAll(Profile profile, Pageable pageable) {
		return repository.findAll(where(profile), pageable);
	}


	private BooleanBuilder where(Profile profile) {
		QProfile qProfile = QProfile.profile;
		BooleanBuilder where = new BooleanBuilder();

		return where;
	}

	/**
	 *
	 * @param profile
	 */
	@Override
	public void save (Profile profile) {
		repository.save(profile);
		return;
	}
}
