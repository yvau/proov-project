package io.spring.service;

import io.spring.model.Bookmark;
import io.spring.repository.BookmarkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Specifies class AutoIncrementService used to handle methods related to
 * <code>Entities</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service
public class BookmarkServiceImpl implements BookmarkService {

    private final BookmarkRepository repository;

    @Autowired
    public BookmarkServiceImpl(final BookmarkRepository repository) {
        this.repository = repository;
    }

    /**
     *
     * @param bookmark
     */
    @Override
    public void save(final Bookmark bookmark) {
        repository.save(bookmark);
        return;
    }

}
