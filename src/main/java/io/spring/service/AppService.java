package io.spring.service;

import io.spring.model.Profile;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Specifies class AppService used to handle methods related to
 * <code>App</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 10/03/2017
 */
public interface AppService {

    Timestamp getDateTime(Date Datetime);

    boolean isValidDate(String inDate);

    Pageable pageable(Integer page, Integer size, String sorting);

    Long getId(String resource);

    Profile getProfile(HttpServletRequest request);

    // List<GrantedAuthority> getAuthorities(String role);

    // Profile getAuth(HttpServletRequest request);

}

