package io.spring.service;

import com.stripe.Stripe;
import com.stripe.exception.*;
import com.stripe.model.Charge;
import io.spring.object.ChargeRequest;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/1/2017
 */
@Service
public class StripeServiceImpl implements StripeService {

    @Resource(name = "stripeConfiguration")
    private Map<String, String> stripeConfiguration;

    @PostConstruct
    public void init() {
        Stripe.apiKey = stripeConfiguration.get("sk_secret");
    }

    public Charge charge(ChargeRequest chargeRequest) throws AuthenticationException, InvalidRequestException, APIConnectionException, CardException, APIException {
        Map<String, Object> chargeParams = new HashMap<>();
        chargeParams.put("amount", chargeRequest.getAmount());
        chargeParams.put("currency", stripeConfiguration.get("currency"));
        chargeParams.put("description", chargeRequest.getDescription());
        chargeParams.put("source", chargeRequest.getToken());
        return Charge.create(chargeParams);
    }
}
