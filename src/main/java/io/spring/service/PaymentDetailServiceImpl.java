package io.spring.service;

import io.spring.model.PaymentDetail;
import io.spring.repository.PaymentDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Specifies class AutoIncrementService used to handle methods related to
 * <code>Entities</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@Service
public class PaymentDetailServiceImpl implements PaymentDetailService {

    private final PaymentDetailRepository repository;

    @Autowired
    public PaymentDetailServiceImpl(final PaymentDetailRepository repository) {
        this.repository = repository;
    }

    /**
     *
     * @param paymentDetail
     */
    @Override
    public void save(final PaymentDetail paymentDetail) {
        repository.save(paymentDetail);
        return;
    }

}
