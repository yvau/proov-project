package io.spring.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * The persistent class for the proposal_has_view database table.
 * 
 */
@Entity
@Table(name="proposal_has_view")
@NamedQuery(name="ProposalHasView.findAll", query="SELECT p FROM ProposalHasView p")
public class ProposalHasView implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	//bi-directional many-to-one association to Proposal
	@ManyToOne
	@JsonManagedReference
	private Proposal proposal;

	public ProposalHasView() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public Proposal getProposal() {
		return this.proposal;
	}

	public void setProposal(Proposal proposal) {
		this.proposal = proposal;
	}

}