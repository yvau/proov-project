package io.spring.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the proposal database table.
 * 
 */
@Entity
@NamedQuery(name="Proposal.findAll", query="SELECT p FROM Proposal p")
public class Proposal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="age_of_property")
	private String ageOfProperty;

	private Integer bathrooms;

	private Integer bedrooms;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	private Boolean enabled;

	private String features;

	@Column(name="is_furnished")
	private Boolean isFurnished;

	@Column(name="is_urgent")
	private Boolean isUrgent;

	private String name;

	@Column(name="price_maximum")
	private BigDecimal priceMaximum;

	@Column(name="price_minimum")
	private BigDecimal priceMinimum;

	private BigDecimal size;

	private String status;

	@Column(name="type_of_property")
	private String typeOfProperty;

	@Column(name="type_of_proposal")
	private String typeOfProposal;

	//bi-directional many-to-one association to Bookmark
	@OneToMany(mappedBy="proposal")
	@JsonBackReference
	private List<Bookmark> bookmarks;

	//bi-directional many-to-one association to FeedActivity
	@OneToMany(mappedBy="proposal")
	@JsonIgnore
	private List<FeedActivity> feedActivities;

	//bi-directional many-to-many association to Location
	@ManyToMany(cascade = { CascadeType.ALL })
	@JsonManagedReference
	@JoinTable(
		name="proposal_has_location"
		, joinColumns={
			@JoinColumn(name="proposal_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="location_id")
			}
		)
	private List<Location> locations;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	//bi-directional many-to-one association to ProposalStatistic
	@ManyToOne
	@JoinColumn(name="proposal_statistic_id")
	private ProposalStatistic proposalStatistic;

	//bi-directional many-to-one association to ProposalHasProperty
	@OneToMany(mappedBy="proposal")
	private List<ProposalHasProperty> proposalHasProperties;

	//bi-directional many-to-one association to ProposalHasView
	@OneToMany(mappedBy="proposal")
	@JsonBackReference
	private List<ProposalHasView> proposalHasViews;

	public Proposal() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAgeOfProperty() {
		return this.ageOfProperty;
	}

	public void setAgeOfProperty(String ageOfProperty) {
		this.ageOfProperty = ageOfProperty;
	}

	public Integer getBathrooms() {
		return this.bathrooms;
	}

	public void setBathrooms(Integer bathrooms) {
		this.bathrooms = bathrooms;
	}

	public Integer getBedrooms() {
		return this.bedrooms;
	}

	public void setBedrooms(Integer bedrooms) {
		this.bedrooms = bedrooms;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getFeatures() {
		return this.features;
	}

	public void setFeatures(String features) {
		this.features = features;
	}

	public Boolean getIsFurnished() {
		return this.isFurnished;
	}

	public void setIsFurnished(Boolean isFurnished) {
		this.isFurnished = isFurnished;
	}

	public Boolean getIsUrgent() {
		return this.isUrgent;
	}

	public void setIsUrgent(Boolean isUrgent) {
		this.isUrgent = isUrgent;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPriceMaximum() {
		return this.priceMaximum;
	}

	public void setPriceMaximum(BigDecimal priceMaximum) {
		this.priceMaximum = priceMaximum;
	}

	public BigDecimal getPriceMinimum() {
		return this.priceMinimum;
	}

	public void setPriceMinimum(BigDecimal priceMinimum) {
		this.priceMinimum = priceMinimum;
	}

	public BigDecimal getSize() {
		return this.size;
	}

	public void setSize(BigDecimal size) {
		this.size = size;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTypeOfProperty() {
		return this.typeOfProperty;
	}

	public void setTypeOfProperty(String typeOfProperty) {
		this.typeOfProperty = typeOfProperty;
	}

	public String getTypeOfProposal() {
		return this.typeOfProposal;
	}

	public void setTypeOfProposal(String typeOfProposal) {
		this.typeOfProposal = typeOfProposal;
	}

	public List<Bookmark> getBookmarks() {
		return this.bookmarks;
	}

	public void setBookmarks(List<Bookmark> bookmarks) {
		this.bookmarks = bookmarks;
	}

	public Bookmark addBookmark(Bookmark bookmark) {
		getBookmarks().add(bookmark);
		bookmark.setProposal(this);

		return bookmark;
	}

	public Bookmark removeBookmark(Bookmark bookmark) {
		getBookmarks().remove(bookmark);
		bookmark.setProposal(null);

		return bookmark;
	}

	public List<FeedActivity> getFeedActivities() {
		return this.feedActivities;
	}

	public void setFeedActivities(List<FeedActivity> feedActivities) {
		this.feedActivities = feedActivities;
	}

	public FeedActivity addFeedActivity(FeedActivity feedActivity) {
		getFeedActivities().add(feedActivity);
		feedActivity.setProposal(this);

		return feedActivity;
	}

	public FeedActivity removeFeedActivity(FeedActivity feedActivity) {
		getFeedActivities().remove(feedActivity);
		feedActivity.setProposal(null);

		return feedActivity;
	}

	public List<Location> getLocations() {
		return this.locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public ProposalStatistic getProposalStatistic() {
		return this.proposalStatistic;
	}

	public void setProposalStatistic(ProposalStatistic proposalStatistic) {
		this.proposalStatistic = proposalStatistic;
	}

	public List<ProposalHasProperty> getProposalHasProperties() {
		return this.proposalHasProperties;
	}

	public void setProposalHasProperties(List<ProposalHasProperty> proposalHasProperties) {
		this.proposalHasProperties = proposalHasProperties;
	}

	public ProposalHasProperty addProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().add(proposalHasProperty);
		proposalHasProperty.setProposal(this);

		return proposalHasProperty;
	}

	public ProposalHasProperty removeProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().remove(proposalHasProperty);
		proposalHasProperty.setProposal(null);

		return proposalHasProperty;
	}

	public List<ProposalHasView> getProposalHasViews() {
		return this.proposalHasViews;
	}

	public void setProposalHasViews(List<ProposalHasView> proposalHasViews) {
		this.proposalHasViews = proposalHasViews;
	}

	public ProposalHasView addProposalHasView(ProposalHasView proposalHasView) {
		getProposalHasViews().add(proposalHasView);
		proposalHasView.setProposal(this);

		return proposalHasView;
	}

	public ProposalHasView removeProposalHasView(ProposalHasView proposalHasView) {
		getProposalHasViews().remove(proposalHasView);
		proposalHasView.setProposal(null);

		return proposalHasView;
	}

}