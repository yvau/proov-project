package io.spring.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the feed_activity database table.
 * 
 */
@Entity
@Table(name="feed_activity")
@NamedQuery(name="FeedActivity.findAll", query="SELECT f FROM FeedActivity f")
public class FeedActivity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	@Column(name="is_interesting")
	private Boolean isInteresting;

	@Column(name="is_read")
	private Boolean isRead;

	private String verb;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	//bi-directional many-to-one association to Proposal
	@ManyToOne
	private Proposal proposal;

	//bi-directional many-to-one association to ProposalHasProperty
	@ManyToOne
	@JoinColumn(name="proposal_has_property_id")
	private ProposalHasProperty proposalHasProperty;

	public FeedActivity() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Boolean getIsInteresting() {
		return this.isInteresting;
	}

	public void setIsInteresting(Boolean isInteresting) {
		this.isInteresting = isInteresting;
	}

	public Boolean getIsRead() {
		return this.isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}

	public String getVerb() {
		return this.verb;
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public Proposal getProposal() {
		return this.proposal;
	}

	public void setProposal(Proposal proposal) {
		this.proposal = proposal;
	}

	public ProposalHasProperty getProposalHasProperty() {
		return this.proposalHasProperty;
	}

	public void setProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		this.proposalHasProperty = proposalHasProperty;
	}

}