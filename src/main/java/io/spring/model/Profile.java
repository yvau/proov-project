package io.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the profile database table.
 * 
 */
@Entity
@NamedQuery(name="Profile.findAll", query="SELECT p FROM Profile p")
public class Profile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="account_non_expired")
	private Boolean accountNonExpired;

	@Column(name="account_non_locked")
	private Boolean accountNonLocked;

	private String credential;

	@Column(name="credentials_non_expired")
	private Boolean credentialsNonExpired;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	@Column(name="date_of_creation_token")
	private Timestamp dateOfCreationToken;

	private Boolean enabled;

	@Column(name="ip_address")
	private String ipAddress;

	private String password;

	private String role;

	private String token;

	//bi-directional many-to-one association to Bookmark
	@OneToMany(mappedBy="profile")
	@JsonIgnore
	private List<Bookmark> bookmarks;

	//bi-directional many-to-one association to FeedActivity
	@OneToMany(mappedBy="profile")
	@JsonIgnore
	private List<FeedActivity> feedActivities;

	//bi-directional many-to-one association to Payment
	@ManyToOne
	@JsonIgnore
	private Payment payment;

	//bi-directional many-to-one association to ProfileInformation
	@ManyToOne
	@JsonManagedReference
	@JoinColumn(name="profile_information_id")
	private ProfileInformation profileInformation;

	//bi-directional many-to-one association to Property
	@OneToMany(mappedBy="profile")
	@JsonIgnore
	private List<Property> properties;

	//bi-directional many-to-one association to Proposal
	@OneToMany(mappedBy="profile")
	@JsonIgnore
	private List<Proposal> proposals;

	//bi-directional many-to-one association to ProposalHasView
	@OneToMany(mappedBy="profile")
	@JsonIgnore
	private List<ProposalHasView> proposalHasViews;

	//bi-directional many-to-one association to SearchProposal
	@OneToMany(mappedBy="profile")
	@JsonIgnore
	private List<SearchProposal> searchProposals;

	public Profile() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getAccountNonExpired() {
		return this.accountNonExpired;
	}

	public void setAccountNonExpired(Boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public Boolean getAccountNonLocked() {
		return this.accountNonLocked;
	}

	public void setAccountNonLocked(Boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public String getCredential() {
		return this.credential;
	}

	public void setCredential(String credential) {
		this.credential = credential;
	}

	public Boolean getCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}

	public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Timestamp getDateOfCreationToken() {
		return this.dateOfCreationToken;
	}

	public void setDateOfCreationToken(Timestamp dateOfCreationToken) {
		this.dateOfCreationToken = dateOfCreationToken;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public List<Bookmark> getBookmarks() {
		return this.bookmarks;
	}

	public void setBookmarks(List<Bookmark> bookmarks) {
		this.bookmarks = bookmarks;
	}

	public Bookmark addBookmark(Bookmark bookmark) {
		getBookmarks().add(bookmark);
		bookmark.setProfile(this);

		return bookmark;
	}

	public Bookmark removeBookmark(Bookmark bookmark) {
		getBookmarks().remove(bookmark);
		bookmark.setProfile(null);

		return bookmark;
	}

	public List<FeedActivity> getFeedActivities() {
		return this.feedActivities;
	}

	public void setFeedActivities(List<FeedActivity> feedActivities) {
		this.feedActivities = feedActivities;
	}

	public FeedActivity addFeedActivity(FeedActivity feedActivity) {
		getFeedActivities().add(feedActivity);
		feedActivity.setProfile(this);

		return feedActivity;
	}

	public FeedActivity removeFeedActivity(FeedActivity feedActivity) {
		getFeedActivities().remove(feedActivity);
		feedActivity.setProfile(null);

		return feedActivity;
	}

	public Payment getPayment() {
		return this.payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public ProfileInformation getProfileInformation() {
		return this.profileInformation;
	}

	public void setProfileInformation(ProfileInformation profileInformation) {
		this.profileInformation = profileInformation;
	}

	public List<Property> getProperties() {
		return this.properties;
	}

	public void setProperties(List<Property> properties) {
		this.properties = properties;
	}

	public Property addProperty(Property property) {
		getProperties().add(property);
		property.setProfile(this);

		return property;
	}

	public Property removeProperty(Property property) {
		getProperties().remove(property);
		property.setProfile(null);

		return property;
	}

	public List<Proposal> getProposals() {
		return this.proposals;
	}

	public void setProposals(List<Proposal> proposals) {
		this.proposals = proposals;
	}

	public Proposal addProposal(Proposal proposal) {
		getProposals().add(proposal);
		proposal.setProfile(this);

		return proposal;
	}

	public Proposal removeProposal(Proposal proposal) {
		getProposals().remove(proposal);
		proposal.setProfile(null);

		return proposal;
	}

	public List<ProposalHasView> getProposalHasViews() {
		return this.proposalHasViews;
	}

	public void setProposalHasViews(List<ProposalHasView> proposalHasViews) {
		this.proposalHasViews = proposalHasViews;
	}

	public ProposalHasView addProposalHasView(ProposalHasView proposalHasView) {
		getProposalHasViews().add(proposalHasView);
		proposalHasView.setProfile(this);

		return proposalHasView;
	}

	public ProposalHasView removeProposalHasView(ProposalHasView proposalHasView) {
		getProposalHasViews().remove(proposalHasView);
		proposalHasView.setProfile(null);

		return proposalHasView;
	}

	public List<SearchProposal> getSearchProposals() {
		return this.searchProposals;
	}

	public void setSearchProposals(List<SearchProposal> searchProposals) {
		this.searchProposals = searchProposals;
	}

	public SearchProposal addSearchProposal(SearchProposal searchProposal) {
		getSearchProposals().add(searchProposal);
		searchProposal.setProfile(this);

		return searchProposal;
	}

	public SearchProposal removeSearchProposal(SearchProposal searchProposal) {
		getSearchProposals().remove(searchProposal);
		searchProposal.setProfile(null);

		return searchProposal;
	}

}