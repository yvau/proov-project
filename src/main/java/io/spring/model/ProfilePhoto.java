package io.spring.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the profile_photo database table.
 * 
 */
@Entity
@Table(name="profile_photo")
@NamedQuery(name="ProfilePhoto.findAll", query="SELECT p FROM ProfilePhoto p")
public class ProfilePhoto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	private String url;

	private BigDecimal size;

	private String type;

	//bi-directional many-to-one association to ProfileInformation
	@OneToMany(mappedBy="profilePhoto")
	@JsonBackReference
	private List<ProfileInformation> profileInformations;

	public ProfilePhoto() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public BigDecimal getSize() {
		return this.size;
	}

	public void setSize(BigDecimal size) {
		this.size = size;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<ProfileInformation> getProfileInformations() {
		return this.profileInformations;
	}

	public void setProfileInformations(List<ProfileInformation> profileInformations) {
		this.profileInformations = profileInformations;
	}

	public ProfileInformation addProfileInformation(ProfileInformation profileInformation) {
		getProfileInformations().add(profileInformation);
		profileInformation.setProfilePhoto(this);

		return profileInformation;
	}

	public ProfileInformation removeProfileInformation(ProfileInformation profileInformation) {
		getProfileInformations().remove(profileInformation);
		profileInformation.setProfilePhoto(null);

		return profileInformation;
	}

}