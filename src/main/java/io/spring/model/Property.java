package io.spring.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the property database table.
 * 
 */
@Entity
@NamedQuery(name="Property.findAll", query="SELECT p FROM Property p")
public class Property implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	private Integer bathrooms;

	private Integer bedrooms;

	private String characteristics;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	private String description;

	private Boolean enabled;

	private BigDecimal price;

	@Column(name="sale_type")
	private String saleType;

	private BigDecimal size;

	private String status;

	private String type;

	//bi-directional many-to-one association to Bookmark
	@OneToMany(mappedBy="property")
	@JsonBackReference
	private List<Bookmark> bookmarks;

	//bi-directional many-to-one association to Location
	@ManyToOne
	private Location location;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	//bi-directional many-to-one association to PropertyPhoto
	@OneToMany(mappedBy="property")
	private List<PropertyPhoto> propertyPhotos;

	//bi-directional many-to-one association to ProposalHasProperty
	@OneToMany(mappedBy="property")
	private List<ProposalHasProperty> proposalHasProperties;

	public Property() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBathrooms() {
		return this.bathrooms;
	}

	public void setBathrooms(Integer bathrooms) {
		this.bathrooms = bathrooms;
	}

	public Integer getBedrooms() {
		return this.bedrooms;
	}

	public void setBedrooms(Integer bedrooms) {
		this.bedrooms = bedrooms;
	}

	public String getCharacteristics() {
		return this.characteristics;
	}

	public void setCharacteristics(String characteristics) {
		this.characteristics = characteristics;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getEnabled() {
		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getSaleType() {
		return this.saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}

	public BigDecimal getSize() {
		return this.size;
	}

	public void setSize(BigDecimal size) {
		this.size = size;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Bookmark> getBookmarks() {
		return this.bookmarks;
	}

	public void setBookmarks(List<Bookmark> bookmarks) {
		this.bookmarks = bookmarks;
	}

	public Bookmark addBookmark(Bookmark bookmark) {
		getBookmarks().add(bookmark);
		bookmark.setProperty(this);

		return bookmark;
	}

	public Bookmark removeBookmark(Bookmark bookmark) {
		getBookmarks().remove(bookmark);
		bookmark.setProperty(null);

		return bookmark;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public List<PropertyPhoto> getPropertyPhotos() {
		return this.propertyPhotos;
	}

	public void setPropertyPhotos(List<PropertyPhoto> propertyPhotos) {
		this.propertyPhotos = propertyPhotos;
	}

	public PropertyPhoto addPropertyPhoto(PropertyPhoto propertyPhoto) {
		getPropertyPhotos().add(propertyPhoto);
		propertyPhoto.setProperty(this);

		return propertyPhoto;
	}

	public PropertyPhoto removePropertyPhoto(PropertyPhoto propertyPhoto) {
		getPropertyPhotos().remove(propertyPhoto);
		propertyPhoto.setProperty(null);

		return propertyPhoto;
	}

	public List<ProposalHasProperty> getProposalHasProperties() {
		return this.proposalHasProperties;
	}

	public void setProposalHasProperties(List<ProposalHasProperty> proposalHasProperties) {
		this.proposalHasProperties = proposalHasProperties;
	}

	public ProposalHasProperty addProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().add(proposalHasProperty);
		proposalHasProperty.setProperty(this);

		return proposalHasProperty;
	}

	public ProposalHasProperty removeProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().remove(proposalHasProperty);
		proposalHasProperty.setProperty(null);

		return proposalHasProperty;
	}

}