package io.spring.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the city database table.
 * 
 */
@Entity
@NamedQuery(name="City.findAll", query="SELECT c FROM City c")
public class City implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@Column(name="alternate_names")
	private String alternateNames;

	@Column(name="f_code")
	private String fCode;

	private String latitude;

	private String longitude;

	private String name;

	@Column(name="name_ascii")
	private String nameAscii;

	private String timezone;

	//bi-directional many-to-one association to Province
	@ManyToOne
	@JsonManagedReference
	private Province province;

	//bi-directional many-to-one association to Location
	@OneToMany(mappedBy="city")
	@JsonBackReference
	private List<Location> locations;

	public City() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAlternateNames() {
		return this.alternateNames;
	}

	public void setAlternateNames(String alternateNames) {
		this.alternateNames = alternateNames;
	}

	public String getFCode() {
		return this.fCode;
	}

	public void setFCode(String fCode) {
		this.fCode = fCode;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameAscii() {
		return this.nameAscii;
	}

	public void setNameAscii(String nameAscii) {
		this.nameAscii = nameAscii;
	}

	public String getTimezone() {
		return this.timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public Province getProvince() {
		return this.province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public List<Location> getLocations() {
		return this.locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public Location addLocation(Location location) {
		getLocations().add(location);
		location.setCity(this);

		return location;
	}

	public Location removeLocation(Location location) {
		getLocations().remove(location);
		location.setCity(null);

		return location;
	}

}