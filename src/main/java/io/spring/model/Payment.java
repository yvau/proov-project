package io.spring.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the payment database table.
 * 
 */
@Entity
@NamedQuery(name="Payment.findAll", query="SELECT p FROM Payment p")
public class Payment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="count_for_rent")
	private Integer countForRent;

	@Column(name="count_for_sale")
	private Integer countForSale;

	@Column(name="customer_id_paypal")
	private String customerIdPaypal;

	@Column(name="customer_id_stripe")
	private String customerIdStripe;

	private String type;

	//bi-directional many-to-one association to PaymentDetail
	@OneToMany(mappedBy="payment")
	private List<PaymentDetail> paymentDetails;

	//bi-directional many-to-one association to Profile
	@OneToMany(mappedBy="payment")
	private List<Profile> profiles;

	//bi-directional many-to-one association to ProposalHasProperty
	@OneToMany(mappedBy="payment")
	private List<ProposalHasProperty> proposalHasProperties;

	public Payment() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCountForRent() {
		return this.countForRent;
	}

	public void setCountForRent(Integer countForRent) {
		this.countForRent = countForRent;
	}

	public Integer getCountForSale() {
		return this.countForSale;
	}

	public void setCountForSale(Integer countForSale) {
		this.countForSale = countForSale;
	}

	public String getCustomerIdPaypal() {
		return this.customerIdPaypal;
	}

	public void setCustomerIdPaypal(String customerIdPaypal) {
		this.customerIdPaypal = customerIdPaypal;
	}

	public String getCustomerIdStripe() {
		return this.customerIdStripe;
	}

	public void setCustomerIdStripe(String customerIdStripe) {
		this.customerIdStripe = customerIdStripe;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<PaymentDetail> getPaymentDetails() {
		return this.paymentDetails;
	}

	public void setPaymentDetails(List<PaymentDetail> paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public PaymentDetail addPaymentDetail(PaymentDetail paymentDetail) {
		getPaymentDetails().add(paymentDetail);
		paymentDetail.setPayment(this);

		return paymentDetail;
	}

	public PaymentDetail removePaymentDetail(PaymentDetail paymentDetail) {
		getPaymentDetails().remove(paymentDetail);
		paymentDetail.setPayment(null);

		return paymentDetail;
	}

	public List<Profile> getProfiles() {
		return this.profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public Profile addProfile(Profile profile) {
		getProfiles().add(profile);
		profile.setPayment(this);

		return profile;
	}

	public Profile removeProfile(Profile profile) {
		getProfiles().remove(profile);
		profile.setPayment(null);

		return profile;
	}

	public List<ProposalHasProperty> getProposalHasProperties() {
		return this.proposalHasProperties;
	}

	public void setProposalHasProperties(List<ProposalHasProperty> proposalHasProperties) {
		this.proposalHasProperties = proposalHasProperties;
	}

	public ProposalHasProperty addProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().add(proposalHasProperty);
		proposalHasProperty.setPayment(this);

		return proposalHasProperty;
	}

	public ProposalHasProperty removeProposalHasProperty(ProposalHasProperty proposalHasProperty) {
		getProposalHasProperties().remove(proposalHasProperty);
		proposalHasProperty.setPayment(null);

		return proposalHasProperty;
	}

}