package io.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


/**
 * The persistent class for the bookmark database table.
 * 
 */
@Entity
@NamedQuery(name="Bookmark.findAll", query="SELECT b FROM Bookmark b")
public class Bookmark implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	@JsonIgnore
	private Profile profile;

	//bi-directional many-to-one association to Property
	@ManyToOne
	@JsonManagedReference
	private Property property;

	//bi-directional many-to-one association to Proposal
	@ManyToOne
	@JsonManagedReference
	private Proposal proposal;

	public Bookmark() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public Property getProperty() {
		return this.property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public Proposal getProposal() {
		return this.proposal;
	}

	public void setProposal(Proposal proposal) {
		this.proposal = proposal;
	}

}