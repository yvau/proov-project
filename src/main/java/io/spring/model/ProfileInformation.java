package io.spring.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the profile_information database table.
 * 
 */
@Entity
@Table(name="profile_information")
@NamedQuery(name="ProfileInformation.findAll", query="SELECT p FROM ProfileInformation p")
public class ProfileInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="accreditation_number")
	private String accreditationNumber;

	@Column(name="agent_type")
	private String agentType;

	@Column(name="best_way_to_reach_you")
	private String bestWayToReachYou;

	@Column(name="birth_date")
	private String birthDate;

	@Column(name="email_contact")
	private String emailContact;

	@Column(name="first_name")
	private String firstName;

	private String gender;

	@Column(name="home_phone")
	private String homePhone;

	@Column(name="last_name")
	private String lastName;

	@Column(name="name_of_banner")
	private String nameOfBanner;

	@Column(name="office_phone")
	private String officePhone;

	//bi-directional many-to-one association to Profile
	@OneToMany(mappedBy="profileInformation")
	@JsonBackReference
	private List<Profile> profiles;

	//bi-directional many-to-one association to ProfilePhoto
	@ManyToOne
	@JoinColumn(name="profile_photo_id")
	@JsonManagedReference
	private ProfilePhoto profilePhoto;

	public ProfileInformation() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccreditationNumber() {
		return this.accreditationNumber;
	}

	public void setAccreditationNumber(String accreditationNumber) {
		this.accreditationNumber = accreditationNumber;
	}

	public String getAgentType() {
		return this.agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getBestWayToReachYou() {
		return this.bestWayToReachYou;
	}

	public void setBestWayToReachYou(String bestWayToReachYou) {
		this.bestWayToReachYou = bestWayToReachYou;
	}

	public String getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getEmailContact() {
		return this.emailContact;
	}

	public void setEmailContact(String emailContact) {
		this.emailContact = emailContact;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHomePhone() {
		return this.homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNameOfBanner() {
		return this.nameOfBanner;
	}

	public void setNameOfBanner(String nameOfBanner) {
		this.nameOfBanner = nameOfBanner;
	}

	public String getOfficePhone() {
		return this.officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public List<Profile> getProfiles() {
		return this.profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public Profile addProfile(Profile profile) {
		getProfiles().add(profile);
		profile.setProfileInformation(this);

		return profile;
	}

	public Profile removeProfile(Profile profile) {
		getProfiles().remove(profile);
		profile.setProfileInformation(null);

		return profile;
	}

	public ProfilePhoto getProfilePhoto() {
		return this.profilePhoto;
	}

	public void setProfilePhoto(ProfilePhoto profilePhoto) {
		this.profilePhoto = profilePhoto;
	}

}