package io.spring.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the search_proposal database table.
 * 
 */
@Entity
@Table(name="search_proposal")
@NamedQuery(name="SearchProposal.findAll", query="SELECT s FROM SearchProposal s")
public class SearchProposal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="category_of_property")
	private String categoryOfProperty;

	@Column(name="has_garage")
	private Boolean hasGarage;

	@Column(name="has_lift")
	private Boolean hasLift;

	@Column(name="has_parking")
	private Boolean hasParking;

	@Column(name="has_pool")
	private Boolean hasPool;

	@Column(name="is_first_buyer")
	private String isFirstBuyer;

	@Column(name="is_furnished")
	private String isFurnished;

	@Column(name="is_near_navigable_body_of_water")
	private Boolean isNearNavigableBodyOfWater;

	@Column(name="is_near_parc")
	private String isNearParc;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	@Column(name="amenity")
	private String amenity;

	@Column(name="is_near_public_transport")
	private Boolean isNearPublicTransport;

	@Column(name="is_near_trade_square")
	private Boolean isNearTradeSquare;

	@Column(name="is_pre_approved")
	private String isPreApproved;

	@Column(name="is_pre_qualified")
	private Boolean isPreQualified;

	@Column(name="is_suitable_for_reduced_mobility")
	private String isSuitableForReducedMobility;

	@Column(name="is_urgent")
	private String isUrgent;

	@Column(name="is_without_contingency")
	private Boolean isWithoutContingency;

	@Column(name="price_maximum")
	private BigDecimal priceMaximum;

	@Column(name="price_minimum")
	private BigDecimal priceMinimum;

	@Lob
	@Type(type = "org.hibernate.type.TextType")
	@Column(name="type_of_property")
	private String typeOfProperty;

	@Column(name="type_of_proposal")
	private String typeOfProposal;

	@Column(name="type_of_search")
	private String typeOfSearch;

	//bi-directional many-to-many association to Location
	@ManyToMany
	@JoinTable(
		name="search_has_location"
		, joinColumns={
			@JoinColumn(name="search_proposal_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="location_id")
			}
		)
	private List<Location> locations;

	//bi-directional many-to-one association to Profile
	@ManyToOne
	private Profile profile;

	public SearchProposal() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategoryOfProperty() {
		return this.categoryOfProperty;
	}

	public void setCategoryOfProperty(String categoryOfProperty) {
		this.categoryOfProperty = categoryOfProperty;
	}

	public Boolean getHasGarage() {
		return this.hasGarage;
	}

	public void setHasGarage(Boolean hasGarage) {
		this.hasGarage = hasGarage;
	}

	public Boolean getHasLift() {
		return this.hasLift;
	}

	public void setHasLift(Boolean hasLift) {
		this.hasLift = hasLift;
	}

	public Boolean getHasParking() {
		return this.hasParking;
	}

	public void setHasParking(Boolean hasParking) {
		this.hasParking = hasParking;
	}

	public Boolean getHasPool() {
		return this.hasPool;
	}

	public void setHasPool(Boolean hasPool) {
		this.hasPool = hasPool;
	}

	public String getIsFirstBuyer() {
		return isFirstBuyer;
	}

	public void setIsFirstBuyer(String isFirstBuyer) {
		this.isFirstBuyer = isFirstBuyer;
	}

	public String getIsPreApproved() {
		return isPreApproved;
	}

	public void setIsPreApproved(String isPreApproved) {
		this.isPreApproved = isPreApproved;
	}

	public String getIsFurnished() {
		return isFurnished;
	}

	public void setIsFurnished(String isFurnished) {
		this.isFurnished = isFurnished;
	}

	public Boolean getIsNearNavigableBodyOfWater() {
		return this.isNearNavigableBodyOfWater;
	}

	public void setIsNearNavigableBodyOfWater(Boolean isNearNavigableBodyOfWater) {
		this.isNearNavigableBodyOfWater = isNearNavigableBodyOfWater;
	}

	public String getIsNearParc() {
		return this.isNearParc;
	}

	public void setIsNearParc(String isNearParc) {
		this.isNearParc = isNearParc;
	}

	public String getAmenity() {
		return amenity;
	}

	public void setAmenity(String amenity) {
		this.amenity = amenity;
	}

	public Boolean getIsNearPublicTransport() {
		return this.isNearPublicTransport;
	}

	public void setIsNearPublicTransport(Boolean isNearPublicTransport) {
		this.isNearPublicTransport = isNearPublicTransport;
	}

	public Boolean getIsNearTradeSquare() {
		return this.isNearTradeSquare;
	}

	public void setIsNearTradeSquare(Boolean isNearTradeSquare) {
		this.isNearTradeSquare = isNearTradeSquare;
	}


	public Boolean getIsPreQualified() {
		return this.isPreQualified;
	}

	public void setIsPreQualified(Boolean isPreQualified) {
		this.isPreQualified = isPreQualified;
	}

	public String getIsSuitableForReducedMobility() {
		return this.isSuitableForReducedMobility;
	}

	public void setIsSuitableForReducedMobility(String isSuitableForReducedMobility) {
		this.isSuitableForReducedMobility = isSuitableForReducedMobility;
	}

	public String getIsUrgent() {
		return isUrgent;
	}

	public void setIsUrgent(String isUrgent) {
		this.isUrgent = isUrgent;
	}

	public Boolean getIsWithoutContingency() {
		return this.isWithoutContingency;
	}

	public void setIsWithoutContingency(Boolean isWithoutContingency) {
		this.isWithoutContingency = isWithoutContingency;
	}

	public BigDecimal getPriceMaximum() {
		return this.priceMaximum;
	}

	public void setPriceMaximum(BigDecimal priceMaximum) {
		this.priceMaximum = priceMaximum;
	}

	public BigDecimal getPriceMinimum() {
		return this.priceMinimum;
	}

	public void setPriceMinimum(BigDecimal priceMinimum) {
		this.priceMinimum = priceMinimum;
	}

	public String getTypeOfProperty() {
		return this.typeOfProperty;
	}

	public void setTypeOfProperty(String typeOfProperty) {
		this.typeOfProperty = typeOfProperty;
	}

	public String getTypeOfProposal() {
		return this.typeOfProposal;
	}

	public void setTypeOfProposal(String typeOfProposal) {
		this.typeOfProposal = typeOfProposal;
	}

	public String getTypeOfSearch() {
		return this.typeOfSearch;
	}

	public void setTypeOfSearch(String typeOfSearch) {
		this.typeOfSearch = typeOfSearch;
	}

	public List<Location> getLocations() {
		return this.locations;
	}

	public void setLocations(List<Location> locations) {
		this.locations = locations;
	}

	public Profile getProfile() {
		return this.profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}