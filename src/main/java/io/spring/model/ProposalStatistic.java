package io.spring.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the proposal_statistic database table.
 * 
 */
@Entity
@Table(name="proposal_statistic")
@NamedQuery(name="ProposalStatistic.findAll", query="SELECT p FROM ProposalStatistic p")
public class ProposalStatistic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="count_proposition")
	private String countProposition;

	@Column(name="count_view")
	private String countView;

	//bi-directional many-to-one association to Proposal
	@OneToMany(mappedBy="proposalStatistic")
	private List<Proposal> proposals;

	public ProposalStatistic() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountProposition() {
		return this.countProposition;
	}

	public void setCountProposition(String countProposition) {
		this.countProposition = countProposition;
	}

	public String getCountView() {
		return this.countView;
	}

	public void setCountView(String countView) {
		this.countView = countView;
	}

	public List<Proposal> getProposals() {
		return this.proposals;
	}

	public void setProposals(List<Proposal> proposals) {
		this.proposals = proposals;
	}

	public Proposal addProposal(Proposal proposal) {
		getProposals().add(proposal);
		proposal.setProposalStatistic(this);

		return proposal;
	}

	public Proposal removeProposal(Proposal proposal) {
		getProposals().remove(proposal);
		proposal.setProposalStatistic(null);

		return proposal;
	}

}