package io.spring.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the proposal_has_property database table.
 * 
 */
@Entity
@Table(name="proposal_has_property")
@NamedQuery(name="ProposalHasProperty.findAll", query="SELECT p FROM ProposalHasProperty p")
public class ProposalHasProperty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Long id;

	@Column(name="date_of_creation")
	private Timestamp dateOfCreation;

	@Column(name="is_interesting")
	private Boolean isInteresting;

	//bi-directional many-to-one association to FeedActivity
	@OneToMany(mappedBy="proposalHasProperty")
	private List<FeedActivity> feedActivities;

	//bi-directional many-to-one association to Payment
	@ManyToOne
	private Payment payment;

	//bi-directional many-to-one association to Property
	@ManyToOne
	private Property property;

	//bi-directional many-to-one association to Proposal
	@ManyToOne
	private Proposal proposal;

	public ProposalHasProperty() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDateOfCreation() {
		return this.dateOfCreation;
	}

	public void setDateOfCreation(Timestamp dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}

	public Boolean getIsInteresting() {
		return this.isInteresting;
	}

	public void setIsInteresting(Boolean isInteresting) {
		this.isInteresting = isInteresting;
	}

	public List<FeedActivity> getFeedActivities() {
		return this.feedActivities;
	}

	public void setFeedActivities(List<FeedActivity> feedActivities) {
		this.feedActivities = feedActivities;
	}

	public FeedActivity addFeedActivity(FeedActivity feedActivity) {
		getFeedActivities().add(feedActivity);
		feedActivity.setProposalHasProperty(this);

		return feedActivity;
	}

	public FeedActivity removeFeedActivity(FeedActivity feedActivity) {
		getFeedActivities().remove(feedActivity);
		feedActivity.setProposalHasProperty(null);

		return feedActivity;
	}

	public Payment getPayment() {
		return this.payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public Property getProperty() {
		return this.property;
	}

	public void setProperty(Property property) {
		this.property = property;
	}

	public Proposal getProposal() {
		return this.proposal;
	}

	public void setProposal(Proposal proposal) {
		this.proposal = proposal;
	}

}