package io.spring.validator;

import com.google.common.base.Strings;
import io.spring.constant.Numeric;
import io.spring.model.Profile;
import io.spring.object.CredentialPassword;
import io.spring.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ChangePasswordValidator implements Validator{

	@Autowired
	private ProfileService profileService;
	
	@Override
	public boolean supports(Class<?> c) {
		//just validate the profiler instances
	        return CredentialPassword.class.isAssignableFrom(c);
	}
	
	@Override
	public void validate(Object obj, Errors errors) {
		CredentialPassword credentialPassword = (CredentialPassword)obj;

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		Profile profile = profileService.getByCredential(currentPrincipalName).get();
		
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "oldPassword",
			    "label.required.is"); 
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
				    "label.required.is"); 
		  
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rePassword",
				    "label.required.is");
		
		
		boolean matched = BCrypt.checkpw(credentialPassword.getOldPassword(), profile.getPassword());
		
		 if (!Strings.isNullOrEmpty(credentialPassword.getOldPassword())) {
			  if (!matched) {  
				  errors.rejectValue("oldPassword", "label.password.current.must.match");
			  }
		  }
		  if (!(Strings.isNullOrEmpty(credentialPassword.getPassword()) && Strings.isNullOrEmpty(credentialPassword.getRePassword()))) {
			  if(!(credentialPassword.getPassword().equals(credentialPassword.getRePassword()))){
				  errors.rejectValue("rePassword", "label.password.dont.match");
			  }
		  }

		  
		  if (!Strings.isNullOrEmpty(credentialPassword.getPassword())) {
			  if (credentialPassword.getPassword().trim().length() < Numeric.MINIMUMLENGTH) {
				  errors.rejectValue("password", "label.password.short");
			  }
		  }
		  
	}
	
}