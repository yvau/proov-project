package io.spring.validator;

import com.google.common.base.Strings;
import io.spring.constant.Variables;
import io.spring.model.Profile;
import io.spring.object.CredentialPassword;
import io.spring.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Specifies class RecoverValidator used to handle methods related to
 * <code>Recover validator</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@Component
public class RecoverValidator implements Validator {

    private Pattern pattern;
    private Matcher matcher;

    @Autowired
    private ProfileService profileService;

    @Override
    public boolean supports(Class<?> c) {
        //just validate the profiler instances
        return Profile.class.isAssignableFrom(c);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        CredentialPassword credentialPassword = (CredentialPassword) obj;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "credential",
                "is.required");

        String credential = credentialPassword.getCredential();

        // email validation in spring
        if (!Strings.isNullOrEmpty(credential)) {
            pattern = Pattern.compile(Variables.EMAIL_PATTERN);
            matcher = pattern.matcher(credential);
            if (!matcher.matches()) {
                errors.rejectValue("credential", "is.incorrect");
            } else if (!profileService.getByCredential(credential).isPresent()) {
                errors.rejectValue("credential", "not.exist");
            }
        }

    }
}
