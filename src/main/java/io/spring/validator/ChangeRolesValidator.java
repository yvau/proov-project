package io.spring.validator;

import io.spring.object.CredentialPassword;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Specifies class ChangeBasicsValidator used to handle methods related to
 * <code>Basic validator</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 5/9/2017
 */
@Component
public class ChangeRolesValidator implements Validator {

    @Override
    public boolean supports(Class<?> c) {
        //just validate the profiler instances
        return CredentialPassword.class.isAssignableFrom(c);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        CredentialPassword credentialPassword = (CredentialPassword) obj;

        /**
         * check if the important value as role is filled up
         */
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "role",
                "is.required");

    }
}