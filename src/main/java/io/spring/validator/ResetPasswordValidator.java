package io.spring.validator;

import com.google.common.base.Strings;
import io.spring.constant.Variables;
import io.spring.object.CredentialPassword;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Specifies class ResetPasswordValidator used to handle methods related to
 * <code>Reset Password validator</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@Component
public class ResetPasswordValidator implements Validator {

    private Pattern pattern;
    private Matcher matcher;

    @Override
    public boolean supports(Class<?> c) {
        //just validate the profiler instances
        return CredentialPassword.class.isAssignableFrom(c);
    }


    @Override
    public void validate(Object obj, Errors errors) {

        CredentialPassword credentialPassword = (CredentialPassword) obj;

        /**
         * check if the important values as password, rePassword are filled up
         */
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
                "is.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rePassword",
                "is.required");

        /**
         * check if the password and rePassword match
         */
        if (!Strings.isNullOrEmpty(credentialPassword.getPassword()) || !Strings.isNullOrEmpty(credentialPassword.getRePassword())) {
            if (!(credentialPassword.getPassword().equals(credentialPassword.getRePassword()))) {
                errors.rejectValue("rePassword", "password.match");
            }
        }

        /**
         * (1) if the user filled up the password field
         *     we check if password match the pattern for password Uppercase,lowercase,number
         *     at least one of each for 6 characters
         * (2) if the password doesn't match the pattern reject the value
         */
        if (!Strings.isNullOrEmpty(credentialPassword.getPassword())) { // (1)
            pattern = Pattern.compile(Variables.PASSWORD_PATTERN);
            matcher = pattern.matcher(credentialPassword.getPassword());

            if (!matcher.matches()) { // (2)
                errors.rejectValue("password", "password.incorrect");
            }
        }


    }
}
