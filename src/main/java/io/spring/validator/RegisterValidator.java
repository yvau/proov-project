package io.spring.validator;

import com.google.common.base.Strings;
import io.spring.constant.Variables;
import io.spring.object.CredentialPassword;
import io.spring.service.AppService;
import io.spring.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Specifies class RegisterValidator used to handle methods related to
 * <code>Register validator</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@Component
public class RegisterValidator implements Validator {

    @Autowired
    ProfileService profileService;

    @Autowired
    AppService appService;

    private Pattern pattern;
    private Matcher matcher;


    @Override
    public boolean supports(Class<?> c) {
        //just validate the profiler instances
        return CredentialPassword.class.isAssignableFrom(c);
    }


    @Override
    public void validate(Object obj, Errors errors) {

        CredentialPassword credentialPassword = (CredentialPassword) obj;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "credential",
                "is.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName",
                "is.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
                "is.required");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "rePassword",
                "is.required");

        if (!appService.isValidDate(credentialPassword.getBirthDate())) {
            errors.rejectValue("birthDate", "date.format.wrong");
        }

        // email validation in spring
        if (!Strings.isNullOrEmpty(credentialPassword.getCredential())) {
            pattern = Pattern.compile(Variables.EMAIL_PATTERN);
            matcher = pattern.matcher(credentialPassword.getCredential());
            if (!matcher.matches()) {
                errors.rejectValue("credential", "credential.incorrect");
            } else if (profileService.getByCredential(credentialPassword.getCredential()).isPresent()) {
                errors.rejectValue("credential", "credential.found");
            }
        }

        // profile password length min, match password = rePassword
        if (!Strings.isNullOrEmpty(credentialPassword.getPassword())) {
            pattern = Pattern.compile(Variables.PASSWORD_PATTERN);
            matcher = pattern.matcher(credentialPassword.getPassword());

            if (!matcher.matches()) {
                errors.rejectValue("password", "password.incorrect");
            }
            if (!(credentialPassword.getPassword().equals(credentialPassword.getRePassword()))) {
                errors.rejectValue("rePassword", "password.match");
            }
        }


    }

}
