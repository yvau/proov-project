package io.spring.validator;

import com.google.common.base.Strings;
import io.spring.enumeration.EnumLabel.BestWayToReach;
import io.spring.object.ContactObject;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Specifies class ChangeContactsValidator used to handle methods related to
 * <code>Contact validator</code>
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 5/9/2017
 */
@Component
public class ChangeContactsValidator implements Validator {

    @Override
    public boolean supports(Class<?> c){
        //just validate the profiler instances
        return ContactObject.class.isAssignableFrom(c);

    }

    @Override
    public void validate(Object obj, Errors errors) {

        ContactObject contactObject = (ContactObject) obj;

        /**
         * check if the important values as emailContacts, BestWayToReach are filled up
         */
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bestWayToReachYou", "is.required");

        /**
         * check if the value entered by the user is
         * first check if BestWayToReach = 'email' then the email field should be filled up
         * second check if BestWayToReach = 'home' then home field should be filled up
         * third check id BestWayToReach = 'office' then office should be filled up
         */
        if (!Strings.isNullOrEmpty(contactObject.getBestWayToReachYou())) {
            if (contactObject.getBestWayToReachYou().equals(BestWayToReach.email.toString()) && Strings.isNullOrEmpty(contactObject.getEmailContact())) {
                errors.rejectValue("emailContact", "is.required");
            } else if (contactObject.getBestWayToReachYou().equals(BestWayToReach.home.toString()) && Strings.isNullOrEmpty(contactObject.getHomePhone())){
                errors.rejectValue("homePhone", "is.required");
            } else if (contactObject.getBestWayToReachYou().equals(BestWayToReach.office.toString()) && Strings.isNullOrEmpty(contactObject.getOfficePhone())){
                errors.rejectValue("officePhone", "is.required");
            }
        }


    }
}
