package io.spring.config;

/**
 * Specifies class used to gather multiples variables related to
 * <code>Application profiles</code>
 * (DEVELOPMENT, TEST, PROD)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public class ApplicationProfiles {
    public static final String DEVELOPMENT = "dev";
    public static final String TEST = "test";
    public static final String PRODUCTION = "prod";
}
