package io.spring.config;

import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.core.config.DefaultConfiguration;
import org.ehcache.expiry.Duration;
import org.ehcache.expiry.Expirations;
import org.ehcache.jsr107.EhcacheCachingProvider;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import javax.cache.CacheManager;
import javax.cache.Caching;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CacheConfiguration extends CachingConfigurerSupport {

    @Bean
    @Override
    public org.springframework.cache.CacheManager cacheManager() {
        // cacheManager = env.acceptsProfiles(ApplicationProfiles.PRODUCTION) ? createClusteredCacheManager() : createInMemoryCacheManager();
        return new JCacheCacheManager(cacheManagerCluster());
    }

    @PreDestroy
    public void destroy() {
        // log.info("Close Cache Manager");
        cacheManagerCluster().close();
    }

    private CacheManager cacheManagerCluster() {
        org.ehcache.config.CacheConfiguration<Object, Object> cacheConfiguration = CacheConfigurationBuilder.newCacheConfigurationBuilder(
                Object.class,
                Object.class,
                ResourcePoolsBuilder.heap(5)).
                withExpiry(Expirations.timeToLiveExpiration(new Duration(5, TimeUnit.SECONDS))).
                build();

        // Map<String, CacheConfiguration> caches = ["person": cacheConfiguration];
        Map<String, org.ehcache.config.CacheConfiguration<?, ?>> caches = createCacheConfigurations(cacheConfiguration);

        EhcacheCachingProvider provider = (EhcacheCachingProvider) Caching.getCachingProvider();
        DefaultConfiguration configuration = new DefaultConfiguration(caches, provider.getDefaultClassLoader());

        return provider.getCacheManager(provider.getDefaultURI(), configuration);
    }

    /* private CacheManager createInMemoryCacheManager() {
        long cacheSize = properties.getCache().getEhcache().getSize();
        long ttl = properties.getCache().getEhcache().getTimeToLiveSeconds();

        org.ehcache.config.CacheConfiguration<Object, Object> cacheConfiguration = CacheConfigurationBuilder
            .newCacheConfigurationBuilder(Object.class, Object.class, ResourcePoolsBuilder
                .newResourcePoolsBuilder()
                .heap(cacheSize))
            .withExpiry(Expirations.timeToLiveExpiration(new org.ehcache.expiry.Duration(ttl, TimeUnit.SECONDS)))
            .build();

        Map<String, org.ehcache.config.CacheConfiguration<?, ?>> caches = createCacheConfigurations(cacheConfiguration);

        EhcacheCachingProvider provider = getCachingProvider();
        DefaultConfiguration configuration = new DefaultConfiguration(caches, provider.getDefaultClassLoader());
        return provider.getCacheManager(provider.getDefaultURI(), configuration);
    } */

    /*private CacheManager createClusteredCacheManager() {
        JHipsterProperties.Cache.Ehcache.Cluster clusterProperties = properties.getCache().getEhcache().getCluster();
        URI clusterUri = clusterProperties.getUri();
        boolean autoCreate = clusterProperties.isAutoCreate();
        long clusteredCacheSize = clusterProperties.getSizeInMb();
        Consistency consistency = clusterProperties.getConsistency();

        long heapCacheSize = properties.getCache().getEhcache().getSize();
        long ttl = properties.getCache().getEhcache().getTimeToLiveSeconds();

        ClusteringServiceConfigurationBuilder clusteringServiceConfigurationBuilder = ClusteringServiceConfigurationBuilder.cluster(clusterUri);
        ServerSideConfigurationBuilder serverSideConfigurationBuilder = (autoCreate ? clusteringServiceConfigurationBuilder.autoCreate() : clusteringServiceConfigurationBuilder.expecting())
            .defaultServerResource("primary-server-resource");

        org.ehcache.config.CacheConfiguration<Object, Object> cacheConfiguration = CacheConfigurationBuilder
            .newCacheConfigurationBuilder(Object.class, Object.class, ResourcePoolsBuilder
                .newResourcePoolsBuilder()
                .heap(heapCacheSize)
                .with(ClusteredResourcePoolBuilder.clusteredDedicated(clusteredCacheSize, MemoryUnit.MB)))
            .withExpiry(Expirations.timeToLiveExpiration(new org.ehcache.expiry.Duration(ttl, TimeUnit.SECONDS)))
            .add(new ClusteredStoreConfiguration(consistency)).build();

        Map<String, org.ehcache.config.CacheConfiguration<?, ?>> caches = createCacheConfigurations(cacheConfiguration);

        EhcacheCachingProvider provider = getCachingProvider();
        DefaultConfiguration configuration = new DefaultConfiguration(caches, provider.getDefaultClassLoader(), serverSideConfigurationBuilder.build());
        return provider.getCacheManager(provider.getDefaultURI(), configuration);
    }*/

    private Map<String, org.ehcache.config.CacheConfiguration<?, ?>> createCacheConfigurations(org.ehcache.config.CacheConfiguration<Object, Object> cacheConfiguration) {
        Map<String, org.ehcache.config.CacheConfiguration<?, ?>> caches = new HashMap<>();
        caches.put("proposals", cacheConfiguration);
        return caches;
    }

    /*private EhcacheCachingProvider getCachingProvider() {
        return (EhcacheCachingProvider) Caching.getCachingProvider();
    }*/
}
