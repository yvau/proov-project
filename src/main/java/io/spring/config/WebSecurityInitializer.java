package io.spring.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
/**
 * @author Yvau
 * @version 1.0
 * @date 12/14/2017
 */
public class WebSecurityInitializer  extends AbstractSecurityWebApplicationInitializer {
}
