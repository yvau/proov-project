package io.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.script.ScriptTemplateConfigurer;
import org.springframework.web.servlet.view.script.ScriptTemplateViewResolver;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/24/2017
 */
@Configuration
public class ViewConfiguration {

    /**
     * Configures where to find the views that we can render. Since we
     * actually do all the rendering in 'render.js', we only have a single
     * placeholder file.
     */
    @Bean
    public ViewResolver renderViewResolver() {
        ScriptTemplateViewResolver viewResolver = new ScriptTemplateViewResolver();
        viewResolver.setPrefix("/WEB-INF/html/");
        viewResolver.setSuffix(".txt");

        return viewResolver;
    }

    /**
     * Configures the {@link javax.script.ScriptEngine} that will render our views.
     */
    @Bean
    public ScriptTemplateConfigurer jsRenderConfigurer() {
        ScriptTemplateConfigurer configurer = new ScriptTemplateConfigurer();
        configurer.setEngineName("nashorn");
		/* Initialise the ScriptEngine with these scripts. */
        configurer.setScripts(getScripts());
		/* The ScriptEngine will call this function to perform the render */
        configurer.setRenderFunction("renderView");
		/* We cannot share a ScriptEngine between threads when rendering React applications */
        configurer.setSharedEngine(true);

        return configurer;
    }

    /**
     * These are the scripts needed to render our React application.
     * <ul>
     *   <li><code>renderer.js</code> - code that renders the response, along with polyfills for some standard functions from a browser / NodeJS environment, and the contents of asset-manifest.json.</li>
     *   <li><code>main.[hash].js</code> - all our application code, bundled up by Webpack, with a hashcode in the name</li>
     * </ul>
     */
    private String[] getScripts() {
        return new String[] {
                "/render/_event-loop.js",
                "/render/_nashorn-polyfill.js",
                "/render/vue.js",
                "/render/basic.js",
                "/render/index.js"
        };
    }
}

