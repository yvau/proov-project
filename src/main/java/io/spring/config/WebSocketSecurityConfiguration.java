package io.spring.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;

/**
 * Specifies class WebSocketSecurityConfiguration used to handle methods related to
 * <code>Web Socket Security Config</code>
 * configureInbound, sameOriginDisabled
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@Configuration
public class WebSocketSecurityConfiguration extends AbstractSecurityWebSocketMessageBrokerConfigurer {

    /**
     *
     * @param messages
     */
    @Override
    protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
        messages
                .simpMessageDestMatchers("/queue/**", "/topic/**").denyAll()
                .simpSubscribeDestMatchers("/queue/**/*-user*", "/topic/**/*-user*").denyAll()
                .simpSubscribeDestMatchers("/user/queue/errors").permitAll()
                .anyMessage().hasAnyRole("SELLER", "BUYER", "TENANT", "LESSOR");
    }

    /**
     *
     * @return
     */
    @Override
    protected boolean sameOriginDisabled() {
        return true;
    }

}