package io.spring.config;

import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * Specifies class SpringWebInitializer used to handle methods related to
 * <code>Web initializer </code>
 * onStartup
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
public class SpringWebInitializer implements WebApplicationInitializer {

    /**
     *
     * @param servletContext
     * @throws ServletException
     */
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        servletContext.setInitParameter("profile", ApplicationProfiles.DEVELOPMENT);
        /**
         * (1) init new <code>{@link AnnotationConfigWebApplicationContext}</code>
         *  register {@link MvcConfiguration}
         */
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext(); // (1)
        rootContext.register(MvcConfiguration.class);
        rootContext.setServletContext(servletContext);

        /**
         * (2) <code> {@link DispatcherServlet}</code>
         * set dispatcher and mapping, set init to throw error <code>throwExceptionIfNoHandlerFound</code>
         * addmapping "/"
         */
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("dispatcher", new DispatcherServlet(rootContext)); // (2)
        dispatcher.setInitParameter("throwExceptionIfNoHandlerFound", Boolean.TRUE.toString());
        dispatcher.setAsyncSupported(Boolean.TRUE);
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");


        /**
         * (3) set one to many , many to one display <code>{@link OpenEntityManagerInViewFilter}</code>
         */
        FilterRegistration.Dynamic filter = servletContext.addFilter("openEntityManagerInViewFilter", OpenEntityManagerInViewFilter.class);
        filter.setInitParameter("singleSession", Boolean.TRUE.toString());
        filter.addMappingForServletNames(null, Boolean.TRUE, "dispatcher");

        /**
         * (4) encoding filter <code>{@link CharacterEncodingFilter}</code>
         */
        FilterRegistration.Dynamic encodingFilter = servletContext.addFilter("encoding-filter", new CharacterEncodingFilter());
        encodingFilter.setInitParameter("encoding", "UTF-8");
        encodingFilter.setInitParameter("forceEncoding", Boolean.TRUE.toString());
        encodingFilter.addMappingForUrlPatterns(null, Boolean.TRUE, "/*");

        /**
         * Add Spring ContextLoaderListener <code>{@link ContextLoaderListener}</code>
         */
        servletContext.addListener(new ContextLoaderListener(rootContext));
    }

}