package io.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Yvau
 * @version 1.0
 * @date 1/19/2018
 */
@Configuration
public class PluginConfiguration {

    @Resource
    Environment env;

    @Bean
    public Map<String, String> cloudinaryConfiguration() {
        final Map<String, String> cloudinaryConfiguration = new HashMap<>();
        cloudinaryConfiguration.put("name", env.getRequiredProperty("cloudinary.cloud.name"));
        cloudinaryConfiguration.put("key", env.getRequiredProperty("cloudinary.api.key"));
        cloudinaryConfiguration.put("secret", env.getRequiredProperty("cloudinary.api.secret"));
        return cloudinaryConfiguration;
    }

    @Bean
    public Map<String, String> stripeConfiguration() {
        final Map<String, String> stripeConfiguration = new HashMap<>();
        stripeConfiguration.put("sk_secret", env.getRequiredProperty("stripe.sk_secret"));
        stripeConfiguration.put("currency", env.getRequiredProperty("stripe.currency"));
        return stripeConfiguration;
    }

    @Bean
    public Map<String, String> paypalConfiguration() {
        final Map<String, String> paypalConfiguration = new HashMap<>();
        paypalConfiguration.put("name", env.getRequiredProperty("cloudinary.cloud.name"));
        paypalConfiguration.put("key", env.getRequiredProperty("cloudinary.api.key"));
        paypalConfiguration.put("secret", env.getRequiredProperty("cloudinary.api.secret"));
        return paypalConfiguration;
    }

    @Bean
    public Map<String, String> emailConfiguration() {
        final Map<String, String> emailConfiguration = new HashMap<>();
        emailConfiguration.put("mail.transport.protocol", env.getRequiredProperty("mail.transport.protocol"));
        emailConfiguration.put("mail.smtp.host", env.getRequiredProperty("mail.smtp.host"));
        emailConfiguration.put("mail.smtp.auth", env.getRequiredProperty("mail.smtp.auth"));
        emailConfiguration.put("mail.smtp.starttls.enable", env.getRequiredProperty("mail.smtp.starttls.enable"));
        emailConfiguration.put("mail.smtp.port", env.getRequiredProperty("mail.smtp.port"));
        emailConfiguration.put("mail.debug", env.getRequiredProperty("mail.debug"));
        emailConfiguration.put("mail.username", env.getRequiredProperty("mail.username"));
        emailConfiguration.put("mail.password", env.getRequiredProperty("mail.password"));
        return emailConfiguration;
    }
}
