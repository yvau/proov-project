package io.spring.web;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static io.spring.common.State.populateModel;

/**
 * Specifies controller used to handle methods related to
 * <code>ProfileActivity</code>
 * (notification, feeds...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@RestController
public class ActivityController {

    /**
     * This method handle notifications for a profile
     *
     * @return notification list interface
     */
    @GetMapping("profile/notification")
    public String notifications(Model model, HttpServletRequest request) {
        //
        populateModel(model, request);
        return "index";
    }
}
