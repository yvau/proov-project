package io.spring.web.rest;

import io.spring.constant.Resources;
import io.spring.constant.Variables;
import io.spring.model.*;
import io.spring.object.Message;
import io.spring.object.ProposalObject;
import io.spring.object.SearchProposalObject;
import io.spring.object.ValidationResponse;
import io.spring.service.*;
import io.spring.validator.ProposalValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Specifies controller used to handle methods related to
 * <code>LessorResource</code>
 * (show, list...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@RestController
@RequestMapping("api/")
public class LessorResource {

    @Autowired
    private AppService appService;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private CityService cityService;

    @Autowired
    private SearchProposalService searchProposalService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private ProposalService proposalService;

    @Autowired
    private ProposalValidator proposalValidator;

    /**
     *
     * @param id
     * @return
     */
    @GetMapping("lessor/{id}")
    public Long show(@PathVariable Long id) {
        //
        return id;
        /*return Optional.ofNullable(proposalService.getById(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new ResourceNotFoundException()
                        .setId(Long.toString(id))
                        .setResourceName(Resources.PROPOSAL));*/
    }

    /**
     *
     * @param searchProposalObject
     * @param bindingResult
     * @return
     */
    @PostMapping("profile/lessor/new")
    // @PreAuthorize("hasAnyRole('ROLE_LESSOR')")
    public ResponseEntity<?> create(SearchProposalObject searchProposalObject, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        // proposalValidator.validate(proposalObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> errorMessages = new ArrayList<>();
            errorMessages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(errorMessages);

            return ResponseEntity.ok(res);
        }

        // Profile profile = profileService.getByCredential("jhyuykhjgh@ihgh.com").get();

        // set and save searchProposal
        SearchProposal searchProposal = new SearchProposal();
        searchProposal.setId(appService.getId(Resources.SEARCH_PROPOSAL));
        // searchProposal.setAgeOfProperty(searchProposalObject.getAgeOfProperty());
        searchProposal.setTypeOfProposal(Variables.FOR_RENT);
        // searchProposal.setIsFirstBuyer(searchProposalObject.getFirstBuyer());
        // searchProposal.setIsUrgent(searchProposalObject.getUrgent());
        // searchProposal.setProfile(profile);
        searchProposalService.save(searchProposal);

        searchProposalObject.getSearchProposalDetails().forEach(details -> {
            // get city object
            City city = cityService.getById(details.getLocation()).get();

            // set and save location
            Location location = new Location();
            location.setId(UUID.randomUUID().toString());
            location.setCity(city);
            location.setProvince(city.getProvince());
            location.setCountry(city.getProvince().getCountry());

        });

        // proposalService.save(proposal);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/proposal/new")
                .buildAndExpand("")
                .toUri();

        return ResponseEntity
                .created(location)
                .body("gfgfd");

    }

    /**
     *
     * @param proposalObject
     * @param bindingResult
     * @return
     */
    @PutMapping("profile/lessor/update")
    public ResponseEntity<?> update(@RequestBody ProposalObject proposalObject, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        proposalValidator.validate(proposalObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> errorMessages = new ArrayList<>();
            errorMessages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(errorMessages);

            return ResponseEntity.ok(res);
        }

        Proposal proposal = proposalService.getById(proposalObject.getId());
        locationService.delete(proposal.getLocations());

        List<Location> asList = new ArrayList<>();

        Arrays.asList(proposalObject.getLocation().split(",")).forEach(ile -> {
            City city = cityService.getById(ile).get();
            Location location = new Location();
            location.setId(UUID.randomUUID().toString()+appService.getId(Resources.LOCATION));
            location.setCity(city);
            location.setProvince(city.getProvince());
            location.setCountry(city.getProvince().getCountry());
            locationService.save(location);
            asList.add(location);
        });

        Profile profile = profileService.getByCredential("jhyuykhjgh@ihgh.com").get();

        proposal.setId(proposal.getId());
        proposal.setAgeOfProperty(proposalObject.getAgeOfProperty());
        proposal.setStatus(proposalObject.getStatus());
        proposal.setIsUrgent(proposalObject.getUrgent());
        proposal.setBedrooms(proposalObject.getBedrooms());
        proposal.setBathrooms(proposalObject.getBathrooms());
        proposal.setSize(proposalObject.getSize());
        proposal.setPriceMaximum(proposalObject.getPriceMaximum());
        proposal.setPriceMinimum(proposalObject.getPriceMinimum());
        proposal.setTypeOfProperty(proposalObject.getTypeOfProperty());
        proposal.setTypeOfProposal(proposalObject.getTypeOfProposal());
        proposal.setIsFurnished(proposalObject.getIsFurnished());
        proposal.setLocations(asList);
        proposal.setProfile(profile);

        proposalService.save(proposal);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body("proposal");
    }

    /**
     *
     * @param id
     * @param proposal
     * @return
     */
    @DeleteMapping("lessor/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id, @RequestBody Proposal proposal) {
        // assertExist(id);
        //  proposalService.save(fileOfProposal);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(proposal);
    }

    /********************************** HELPER METHOD **********************************/
    /*private ResponseEntity<?> assertExist(Long id) {
        return Optional.ofNullable(proposalService.getById(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new ResourceNotFoundException()
                        .setId(Long.toString(id))
                        .setResourceName(Resources.PROPOSAL));
    }*/
}
