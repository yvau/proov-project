package io.spring.web.rest;

import com.google.common.base.Strings;
import io.spring.constant.Numeric;
import io.spring.constant.Resources;
import io.spring.constant.Variables;
import io.spring.contentWrapper.PageWrapper;
import io.spring.model.Property;
import io.spring.model.Proposal;
import io.spring.model.ProposalHasProperty;
import io.spring.object.Message;
import io.spring.object.ProposalHasPropertyObject;
import io.spring.service.AppService;
import io.spring.service.PropertyService;
import io.spring.service.ProposalHasPropertyService;
import io.spring.service.ProposalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author Yvau
 * @version 1.0
 * @date 2/9/2018
 */
@RestController
@RequestMapping("api/")
public class ProposalHasPropertyResource {

    @Autowired
    PropertyService propertyService;

    @Autowired
    ProposalService proposalService;

    @Autowired
    AppService appService;

    @Autowired
    ProposalHasPropertyService proposalHasPropertyService;

    @PostMapping("propose/new")
    public ResponseEntity<?> create(@RequestBody List<ProposalHasPropertyObject> proposalHasProperties) {
        Proposal proposal = proposalService.getById(proposalHasProperties.get(Numeric.NULL_VALUE).getProposal());

        proposalHasProperties.stream().forEach(proposalHasPropertyObject -> {
            Property property = propertyService.getById(proposalHasPropertyObject.getProperty());

            ProposalHasProperty proposalHasProperty = new ProposalHasProperty();
            proposalHasProperty.setId(appService.getId(Resources.PROPOSAL_HAS_PROPERTY));
            proposalHasProperty.setProperty(property);
            proposalHasProperty.setProposal(proposal);
            proposalHasProperty.setIsInteresting(Boolean.TRUE);
            proposalHasProperty.setDateOfCreation(Timestamp.from(Variables.timestamp));
            proposalHasPropertyService.save(proposalHasProperty);
        });

        URI locate = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand("property.getId()")
                .toUri();

        return ResponseEntity
                .created(locate)
                .body(new Message("Well done !", "Your feed has been successfully updated", null));
    }

    /**
     *
     * @param page
     * @param sorting
     * @param proposalId
     * @return
     */
    @GetMapping("propose/list")
    public ResponseEntity<?> list(@RequestParam(value = "page", required = false) Integer page,
                                  @RequestParam(value = "sorting", required = false) String sorting,
                                  @RequestParam(value = "proposal", required = false) String proposalId) {

        ProposalHasProperty proposalHasProperty = new ProposalHasProperty();

        if(!Strings.isNullOrEmpty(proposalId)) {
            Proposal proposal = proposalService.getById(Long.valueOf(proposalId));
            proposalHasProperty.setProposal(proposal);
        }

        // Parse request parameters
        Pageable pageable = appService.pageable(page, null,sorting);

        PageWrapper<ProposalHasProperty> list = new PageWrapper<>(proposalHasPropertyService.findAll(proposalHasProperty, pageable), "/api/property/list" );

        return ResponseEntity
                .ok(list);
    }
}
