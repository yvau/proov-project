package io.spring.web.rest;

import io.spring.constant.Resources;
import io.spring.constant.Variables;
import io.spring.model.Bookmark;
import io.spring.model.Profile;
import io.spring.model.Property;
import io.spring.model.Proposal;
import io.spring.object.BookmarkObject;
import io.spring.object.Message;
import io.spring.security.TokenHelper;
import io.spring.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

/**
 * @author Yvau
 * @version 1.0
 * @date 4/6/2018
 */
@RestController
@RequestMapping("api/")
public class BookmarkResource {

    @Autowired
    AppService appService;

    @Autowired
    ProfileService profileService;

    @Autowired
    ProposalService proposalService;

    @Autowired
    PropertyService propertyService;

    @Autowired
    BookmarkService bookmarkService;

    @Autowired
    private TokenHelper tokenHelper;

    @PostMapping("bookmark/new")
    public ResponseEntity<?> create(@RequestBody BookmarkObject bookmarkObject, HttpServletRequest request) {

        String token = tokenHelper.getToken(request);
        Profile profile = profileService.getByCredential(tokenHelper.getUsernameFromToken(token)).get();

        Bookmark bookmark = new Bookmark();
        bookmark.setId(appService.getId(Resources.BOOKMARK));
        bookmark.setDateOfCreation(appService.getDateTime(Variables.dateTime));
        bookmark.setProfile(profile);

        if (bookmarkObject.getNature().equals(Resources.PROPERTY)) {
            Property property = propertyService.getById(bookmarkObject.getProperty());
            bookmark.setProperty(property);
        } else if (bookmarkObject.getNature().equals(Resources.PROPOSAL)) {
            Proposal proposal = proposalService.getById(bookmarkObject.getProposal());
            bookmark.setProposal(proposal);
        }

        bookmarkService.save(bookmark);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/bookmark/new")
                .buildAndExpand("")
                .toUri();

        return ResponseEntity
                .created(location)
                .body(new Message("label.success", "your bookmark has been successfully saved", null));
    }
}
