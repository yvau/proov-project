package io.spring.web.rest;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import io.spring.constant.Variables;
import io.spring.contentWrapper.PageWrapper;
import io.spring.model.Profile;
import io.spring.model.ProfileInformation;
import io.spring.model.ProfilePhoto;
import io.spring.object.ContactObject;
import io.spring.object.CredentialPassword;
import io.spring.object.Message;
import io.spring.object.ValidationResponse;
import io.spring.service.AppService;
import io.spring.service.ProfileInformationService;
import io.spring.service.ProfilePhotoService;
import io.spring.service.ProfileService;
import io.spring.validator.ChangeBasicsValidator;
import io.spring.validator.ChangeContactsValidator;
import io.spring.validator.ChangePasswordValidator;
import io.spring.validator.ChangeRolesValidator;
import org.cloudinary.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Specifies controller used to handle methods related to
 * <code>ProfileResource</code>
 * (show, list...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 8/30/2017
 */
@RestController
@RequestMapping("api/")
public class ProfileResource {

    @Resource(name = "cloudinaryConfiguration")
    private Map<String, String> cloudinaryConfiguration;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private ChangePasswordValidator changePasswordValidator;

    @Autowired
    private AppService appService;

    @Autowired
    private ChangeBasicsValidator changeBasicsValidator;

    @Autowired
    private ChangeRolesValidator changeRolesValidator;

    @Autowired
    private ChangeContactsValidator changeContactsValidator;

    @Autowired
    private ProfileInformationService profileInformationService;

    @Autowired
    private ProfilePhotoService profilePhotoService;

    /**
     *
     * @param credentialPassword
     * @param bindingResult
     * @return
     */
    @PostMapping("profile/edit/information")
    public ResponseEntity<?> updateInformation(CredentialPassword credentialPassword, BindingResult bindingResult, HttpServletRequest request) {
        ValidationResponse res = new ValidationResponse();
        // pass data to validate form
        changeBasicsValidator.validate(credentialPassword, bindingResult);
        //show error if data are not valid
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        Profile profile = appService.getProfile(request);
        //save data
        ProfileInformation profileInformation = profile.getProfileInformation();
        profileInformation.setFirstName(credentialPassword.getFirstName());
        profileInformation.setLastName(credentialPassword.getLastName());
        profileInformation.setBirthDate(credentialPassword.getBirthDate());
        profileInformation.setGender(credentialPassword.getGender());
        profileInformationService.save(profileInformation);

        if (credentialPassword.getPhoto() != null) {
            Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                    "cloud_name", cloudinaryConfiguration.get("name"),
                    "api_key", cloudinaryConfiguration.get("key"),
                    "api_secret", cloudinaryConfiguration.get("secret")));
            try {
                File f = Files.createTempFile("temp", credentialPassword.getPhoto().getOriginalFilename()).toFile();
                credentialPassword.getPhoto().transferTo(f);
                Map result = cloudinary.uploader().upload(f, ObjectUtils.emptyMap());
                JSONObject json = new JSONObject(result);
                ProfilePhoto profilePhoto =  new ProfilePhoto();
                profilePhoto.setId(profileInformation.getProfilePhoto().getId());
                profilePhoto.setDateOfCreation(Timestamp.from(Variables.timestamp));
                profilePhoto.setUrl(json.getString("secure_url"));
                profilePhoto.setProfileInformations(Arrays.asList(profileInformation));
                profilePhoto.setSize(BigDecimal.valueOf(json.getLong("bytes")));
                profilePhoto.setType(json.getString("format"));
                profilePhotoService.save(profilePhoto);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //return response
        return ResponseEntity.ok(new Message("label.success", "Your informations have been successfully changed", null));
    }

    /**
     *
     * @param credentialPassword
     * @return
     */
    @PutMapping("profile/edit/role")
    public ResponseEntity<?> updateRole(@RequestBody CredentialPassword credentialPassword, BindingResult bindingResult, HttpServletRequest request) {
        ValidationResponse res = new ValidationResponse();
        changeRolesValidator.validate(credentialPassword, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        Profile profile = appService.getProfile(request);
        profile.setRole(credentialPassword.getRole());
        profileService.save(profile);

        // final UserDetails userDetails = userDetailsService.loadUserByUsername("admin");
        // final String token = jwtTokenUtil.generateToken(userDetails, device);

        // Return the token
        return ResponseEntity.ok(new Message("label.success","Your roles have been changed", null));
    }

    /**
     *
     * @param contactObject
     * @param bindingResult
     * @return
     */
    @PutMapping("profile/edit/contacts")
    public ResponseEntity<?> updateContacts(@RequestBody ContactObject contactObject, BindingResult bindingResult, HttpServletRequest request) {
        ValidationResponse res = new ValidationResponse();
        changeContactsValidator.validate(contactObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        ProfileInformation profileInformation = appService.getProfile(request).getProfileInformation();
        profileInformation.setBestWayToReachYou(contactObject.getBestWayToReachYou());
        profileInformation.setOfficePhone(contactObject.getOfficePhone());
        profileInformation.setHomePhone(contactObject.getHomePhone());
        profileInformation.setEmailContact(contactObject.getEmailContact());

        profileInformationService.save(profileInformation);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Message("label.success","Your password has been changed", null));
    }

    @PutMapping("profile/edit/password")
    public ResponseEntity<?> updatePassword(@RequestBody CredentialPassword credentialPassword, BindingResult bindingResult, HttpServletRequest request) {
        ValidationResponse res = new ValidationResponse();
        changePasswordValidator.validate(credentialPassword, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);
            return ResponseEntity.ok(res);
        }
        // JwtUser jwtUser = (JwtUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        // System.out.println("ffewggr " + jwtUser.getUsername());

        Profile profile = appService.getProfile(request);
        profile.setPassword(Variables.PASSWORD_ENCODER.encode(credentialPassword.getPassword()));
        profileService.save(profile);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Message("label.success","Your password has been changed", null));
    }

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Message greeting() throws Exception {
        Thread.sleep(1000); // simulated delay
        return new Message("Hello, !", "fd", null);
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping("account/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        // Parse request parameters

        Optional<Profile> profile = profileService.getById(id);

        return ResponseEntity.ok(profile.get());
    }


    /**
     *
     * @param page
     * @param sorting
     * @param profile
     * @return
     */
    @GetMapping("account/list")
    public ResponseEntity<?> list(@RequestParam(value = "page", required = false) Integer page,
                                  @RequestParam(value = "sorting", required = false) String sorting, Profile profile) {
        // Parse request parameters
        Pageable pageable = appService.pageable(page, null,sorting);

        PageWrapper<Profile> list = new PageWrapper<>(profileService.getAll(profile, pageable), "/profiles" );

        return ResponseEntity
                .ok(list);
    }

    @GetMapping("notifications")
    public ResponseEntity<?> notifications(@RequestParam(value = "page", required = false) Integer page,
                                  @RequestParam(value = "sorting", required = false) String sorting, Profile profile) {
        // Parse request parameters
        Pageable pageable = appService.pageable(page, null, sorting);

        PageWrapper<Profile> list = new PageWrapper<>(profileService.getAll(profile, pageable), "/profiles" );

        return ResponseEntity
                .ok(list);
    }


    /**
     *
     * @param request
     * @return
     */
    @GetMapping("profile/get")
    public ResponseEntity<?> getAuth(HttpServletRequest request) {
        try {
            return ResponseEntity.ok(appService.getProfile(request));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("");
        }
    }
}
