package io.spring.web.rest;

import io.spring.constant.Resources;
import io.spring.constant.Variables;
import io.spring.model.City;
import io.spring.model.Location;
import io.spring.model.SearchProposal;
import io.spring.object.Message;
import io.spring.object.SearchProposalObject;
import io.spring.object.ValidationResponse;
import io.spring.service.AppService;
import io.spring.service.CityService;
import io.spring.service.SearchProposalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Yvau
 * @version 1.0
 * @date 12/11/2017
 */
@Controller
@RequestMapping("api/")
public class SellerResource {

    @Autowired
    private AppService appService;

    @Autowired
    private CityService cityService;

    @Autowired
    private SearchProposalService searchProposalService;

    /**
     *
     * @param id
     * @return
     */
    @GetMapping("seller/{id}")
    public Long show(@PathVariable Long id) {
        //
        return id;
        /*return Optional.ofNullable(proposalService.getById(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new ResourceNotFoundException()
                        .setId(Long.toString(id))
                        .setResourceName(Resources.PROPOSAL));*/
    }


    /**
     *
     * @param searchProposalObject
     * @param bindingResult
     * @return
     */
    @PostMapping("seller/new")
    // @PreAuthorize("hasAnyRole('ROLE_LESSOR')")
    public ResponseEntity<?> create(SearchProposalObject searchProposalObject, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        // proposalValidator.validate(proposalObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> errorMessages = new ArrayList<>();
            errorMessages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(errorMessages);

            return ResponseEntity.ok(res);
        }

        // Profile profile = profileService.getByCredential("jhyuykhjgh@ihgh.com").get();

        // set and save searchProposal
        SearchProposal searchProposal = new SearchProposal();
        searchProposal.setId(appService.getId(Resources.SEARCH_PROPOSAL));
        searchProposal.setIsPreApproved(searchProposalObject.getIsPreApproved());
        searchProposal.setIsFirstBuyer(searchProposalObject.getIsFirstBuyer());
        searchProposal.setIsUrgent(searchProposalObject.getIsUrgent());
        searchProposal.setCategoryOfProperty(searchProposalObject.getCategoryOfProperty());
        searchProposal.setPriceMinimum(searchProposalObject.getPriceMinimum());
        searchProposal.setPriceMaximum(searchProposalObject.getPriceMaximum());
        searchProposal.setTypeOfSearch(Variables.FOR_SALE);
        searchProposal.setTypeOfProposal(Variables.FOR_SALE);

        // searchProposal.setProfile(profile);
        searchProposalService.save(searchProposal);

        if (searchProposalObject.getSearchProposalDetails() != null) {
            searchProposalObject.getSearchProposalDetails().forEach(details -> {
                // get city object
                City city = cityService.getById(details.getLocation()).get();

                // set and save location
                Location location = new Location();
                location.setId(UUID.randomUUID().toString());
                location.setCity(city);
                location.setProvince(city.getProvince());
                location.setCountry(city.getProvince().getCountry());
            });
        }

        // proposalService.save(proposal);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/seller/new")
                .buildAndExpand("")
                .toUri();

        return ResponseEntity
                .created(location)
                .body("gfgfd");

    }
}
