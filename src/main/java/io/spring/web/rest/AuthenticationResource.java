package io.spring.web.rest;

import io.spring.constant.Resources;
import io.spring.constant.Variables;
import io.spring.mail.EmailSender;
import io.spring.mail.MustacheCompiler;
import io.spring.model.Profile;
import io.spring.model.ProfileInformation;
import io.spring.model.UserTokenState;
import io.spring.object.CredentialPassword;
import io.spring.object.Message;
import io.spring.object.ValidationResponse;
import io.spring.security.JwtAuthenticationRequest;
import io.spring.security.TokenHelper;
import io.spring.service.AppService;
import io.spring.service.CustomUserDetailsService;
import io.spring.service.ProfileInformationService;
import io.spring.service.ProfileService;
import io.spring.validator.RecoverValidator;
import io.spring.validator.RegisterValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.security.Principal;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * @author Yvau
 * @version 1.0
 * @date 12/15/2017
 */
@RestController
@RequestMapping( value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE )
public class AuthenticationResource {

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private AppService appService;

    @Autowired
    private ProfileInformationService profileInformationService;

    @Autowired
    private RegisterValidator registerValidator;

    @Autowired
    private RecoverValidator recoverValidator;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private CustomUserDetailsService userDetailsService;


    /**
     *
     * @param authenticationRequest
     * @param response
     * @return
     * @throws AuthenticationException
     * @throws IOException
     */
    @PostMapping("login")
    public ResponseEntity<?> createAuthenticationToken(
            @RequestBody JwtAuthenticationRequest authenticationRequest,
            HttpServletResponse response
    ) throws AuthenticationException, IOException {

        final Optional<Profile> isOptional = profileService.getByCredential(authenticationRequest.getUsername());

        if (!isOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Message("label.error","Authentication Failed. Username or Password not valid.", null));
        }

        if (!passwordEncoder.matches(authenticationRequest.getPassword(), isOptional.get().getPassword())) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new Message("label.error","Authentication Failed. Username or Password not valid.", null));
        }

        // Perform the security
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );

        // Inject into security context
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // token creation
        // User user = (User)authentication.getPrincipal();
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        String jws = tokenHelper.generateToken( userDetails.getUsername());
        int expiresIn = tokenHelper.getExpiredIn();
        // Return the token
        return ResponseEntity.ok(new UserTokenState(jws, expiresIn));
    }

    /**
     *
     * @param credentialPassword
     * @param bindingResult
     * @param request
     * @return
     * @throws MessagingException
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @PostMapping("register")
    public ResponseEntity<?> create(@RequestBody CredentialPassword credentialPassword, BindingResult bindingResult, HttpServletRequest request) {
        ValidationResponse res = new ValidationResponse();
        registerValidator.validate(credentialPassword, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        // set Values to profile Information
        ProfileInformation profileInformation = new ProfileInformation();
        profileInformation.setId(appService.getId(Resources.PROFILE_INFORMATION));
        profileInformation.setFirstName(credentialPassword.getFirstName());
        profileInformation.setLastName(credentialPassword.getLastName());
        profileInformationService.save(profileInformation);

        // set values to profile
        Profile profile = new Profile();
        profile.setId(appService.getId(Resources.PROFILE));
        profile.setPassword(Variables.PASSWORD_ENCODER.encode(credentialPassword.getPassword()));
        profile.setIpAddress(request.getRemoteAddr());
        profile.setToken(Variables.THE_ID + ThreadLocalRandom.current().nextInt(1, 10));
        profile.setDateOfCreationToken(appService.getDateTime(Variables.dateTime));
        profile.setDateOfCreation(appService.getDateTime(Variables.dateTime));
        profile.setCredential(credentialPassword.getCredential());
        profile.setCredentialsNonExpired(Boolean.TRUE);
        profile.setEnabled(Boolean.FALSE);
        profile.setAccountNonExpired(Boolean.TRUE);
        profile.setAccountNonLocked(Boolean.TRUE);
        profile.setRole(Variables.ROLE_LIMITED);
        profile.setProfileInformation(profileInformation);
        profileService.save(profile);

        // send a mail to the profile
        String body = MustacheCompiler.generate(profile, "mail/register.mustache");
        emailSender.send("myemail@here.com", "Welcome to our system!", body);

        // send success message to the user
        Message message = new Message("label.success","An activation link has been sent to " + profile.getCredential() + " please click on the link to activate your account", null);


        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/register")
                .buildAndExpand(profile.getId())
                .toUri();

        return ResponseEntity
                .created(location)
                .body(message);
    }


    /**
     *
     * @param credentialPassword
     * @param bindingResult
     * @return
     */
    @PostMapping("recover")
    public ResponseEntity<?> recover(@RequestBody CredentialPassword credentialPassword, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        recoverValidator.validate(credentialPassword, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        Profile profile = profileService.getByCredential(credentialPassword.getCredential()).get();
        profile.setToken(Variables.THE_ID + ThreadLocalRandom.current().nextInt(1, 10));
        profileService.save(profile);

        // send a mail to the profile
        String body = MustacheCompiler.generate(profile, "mail/recover.mustache");
        emailSender.send("myemail@here.com", "Recover to our system!", body);

        // send success message to the user
        Message message = new Message("label.success","An activation link has been sent to " + profile.getCredential() + " please click on the link to recover your password", null);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/recover")
                .buildAndExpand("")
                .toUri();

        return ResponseEntity
                .created(location)
                .body(message);
    }

    @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    public ResponseEntity<?> refreshAuthenticationToken(
            HttpServletRequest request,
            HttpServletResponse response,
            Principal principal
    ) {

        String authToken = tokenHelper.getToken( request );

        if (authToken != null && principal != null) {

            // TODO check user password last update
            String refreshedToken = tokenHelper.refreshToken(authToken);
            int expiresIn = tokenHelper.getExpiredIn();

            return ResponseEntity.ok(new UserTokenState(refreshedToken, expiresIn));
        } else {
            UserTokenState userTokenState = new UserTokenState();
            return ResponseEntity.accepted().body(userTokenState);
        }
    }

    /**
     *
     * @param token
     * @return
     */
    @GetMapping("activate")
    public ResponseEntity<?> activate(@RequestParam(value = "token") String token){
        Optional<Profile> profile = profileService.getByToken(token);
        if(!profile.isPresent())
            return ResponseEntity.ok(new Message("label.error","the token provided is not valid or has expired", null));

        // Reload password post-security so we can generate token
        final UserDetails userDetails = userDetailsService.loadUserByUsername(profile.get().getCredential());
        String jws = tokenHelper.generateToken( userDetails.getUsername());
        int expiresIn = tokenHelper.getExpiredIn();

        profile.get().setEnabled(Boolean.TRUE);
        profile.get().setToken(null);
        profileService.save(profile.get());

        // Return the token
        return ResponseEntity.ok(new UserTokenState(jws, expiresIn));
    }

    @RequestMapping(value = "/change-password", method = RequestMethod.POST)
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> changePassword(@RequestBody PasswordChanger passwordChanger) {
        // userDetailsService.changePassword(passwordChanger.oldPassword, passwordChanger.newPassword);
        Map<String, String> result = new HashMap<>();
        result.put( "result", "success" );
        return ResponseEntity.accepted().body(result);
    }

    static class PasswordChanger {
        public String oldPassword;
        public String newPassword;
    }
}
