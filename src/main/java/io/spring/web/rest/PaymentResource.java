package io.spring.web.rest;

import com.paypal.api.payments.Links;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import io.spring.constant.Resources;
import io.spring.constant.Variables;
import io.spring.model.PackageProduct;
import io.spring.model.PaymentDetail;
import io.spring.object.ChargeRequest;
import io.spring.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/1/2017
 */
@RestController
@RequestMapping("auth/payment/")
public class PaymentResource {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    AppService appService;

    @Autowired
    PaypalService paypalService;

    @Autowired
    StripeService stripeService;

    @Autowired
    PaymentDetailService paymentDetailService;

    @Autowired
    PackageProductService packageProductService;

    @PostMapping("paypal")
    public ResponseEntity<?> payPalPayment(HttpServletRequest req) {
        Map<String, String> map = new HashMap<>();
        try {
            Payment payment = paypalService.createPayment(399.00,"fewfwe", "dqwdw", req);
            for(Links links : payment.getLinks())
                if (links.getRel().equals(Variables.APPROVAL_URL))
                    map.put(Variables.PAYMENT_ID, payment.getId());
        } catch (PayPalRESTException e){
            log.error(e.getMessage());
        }
        return ResponseEntity.ok(map);
    }

    @GetMapping("cancel")
    public String cancelPay() {
        return "index";
    }

    @PostMapping("payer")
    public ResponseEntity<?> chargePaypal(@RequestParam("paymentID") String paymentId, @RequestParam("payerID") String payerId) {
        try {
            Payment payment = paypalService.executePayment(paymentId, payerId);
            Object[] parameters = new Object[] {payment.getId(), payment.getPayer()};

            if (payment.getState().equals(Variables.APPROVED)) {
                PaymentDetail paymentDetail = new PaymentDetail();
                paymentDetail.setId(appService.getId(Resources.PAYMENT_DETAIL));
                paymentDetail.setIdTransaction(payment.getId());
                paymentDetail.setDateOfCreation(Timestamp.from(Variables.timestamp));
                paymentDetail.setMethod(Variables.PAYPAL);
                // paymentDetail.setPackageProduct(packageProduct);

                paymentDetailService.save(paymentDetail);

                return ResponseEntity.ok(parameters);
            }

        } catch (PayPalRESTException e) {
            log.error(e.getMessage());
        }
        return ResponseEntity.ok("redirect");
    }

    @PostMapping("charge")
    public String chargeCard(@RequestBody ChargeRequest chargeRequest) throws StripeException {
        Charge charge = stripeService.charge(chargeRequest);

        PackageProduct packageProduct = packageProductService.getById(chargeRequest.getPackageProduct()).get();

        PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setId(appService.getId(Resources.PAYMENT_DETAIL));
        paymentDetail.setIdTransaction(charge.getId());
        paymentDetail.setDateOfCreation(Timestamp.from(Variables.timestamp));
        paymentDetail.setMethod(Variables.STRIPE);
        paymentDetail.setPackageProduct(packageProduct);
        return "success";
    }

    /**
     *
     * @param chargeRequest
     * @return
     * @throws StripeException
     */
    @PostMapping("create/card")
    public String createCard(@RequestBody ChargeRequest chargeRequest) throws StripeException {
        Charge charge = stripeService.charge(chargeRequest);

        PackageProduct packageProduct = packageProductService.getById(chargeRequest.getPackageProduct()).get();

        PaymentDetail paymentDetail = new PaymentDetail();
        paymentDetail.setId(appService.getId(Resources.PAYMENT_DETAIL));
        paymentDetail.setIdTransaction(charge.getId());
        paymentDetail.setDateOfCreation(Timestamp.from(Variables.timestamp));
        paymentDetail.setMethod(Variables.STRIPE);
        paymentDetail.setPackageProduct(packageProduct);


        return "success";
    }
}
