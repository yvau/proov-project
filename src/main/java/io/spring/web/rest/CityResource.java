package io.spring.web.rest;

import io.spring.model.City;
import io.spring.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Specifies controller used to handle methods related to
 * <code>CityResource</code>
 * (cities...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@RestController
@RequestMapping("rest/city")
public class CityResource {

    @Autowired
    CityService cityService;

    /**
     *
     * @param q
     * @return
     */
    @GetMapping
    public ResponseEntity<?> cities(@RequestParam(value = "q") String q) {

        // Parse request parameters
        List<City> list = cityService.findByName(q);

        return ResponseEntity
                .ok(list);
    }

}
