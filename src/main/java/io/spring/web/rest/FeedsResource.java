package io.spring.web.rest;

import io.spring.contentWrapper.PageWrapper;
import io.spring.model.FeedActivity;
import io.spring.object.FeedActivityObject;
import io.spring.object.Message;
import io.spring.service.AppService;
import io.spring.service.FeedActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

/**
 * @author Yvau
 * @version 1.0
 * @date 12/28/2017
 */
@RestController
@RequestMapping("api/")
public class FeedsResource {

    @Autowired
    AppService appService;

    @Autowired
    FeedActivityService feedActivityService;


    @PutMapping("feed/update")
    public ResponseEntity<?> update(@RequestBody FeedActivityObject feedActivityObject) {

        Boolean isInteresting = (feedActivityObject.getIsInteresting().equals("false"))? Boolean.FALSE : Boolean.TRUE;
        FeedActivity feedActivity = feedActivityService.getById(feedActivityObject.getId()).get();
        feedActivity.setIsInteresting(isInteresting);
        feedActivityService.save(feedActivity);

        URI locate = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(feedActivity.getId())
                .toUri();

        return ResponseEntity
                .created(locate)
                .body(new Message("Well done !", "Your feed has been successfully updated", null));
    }

    /**
     *
     * @param page
     * @param sorting
     * @param feedActivity
     * @return
     */
    @GetMapping("feed/list")
    public ResponseEntity<?> list(@RequestParam(value = "page", required = false) Integer page,
                                  @RequestParam(value = "sorting", required = false) String sorting,
                                  @RequestParam(value = "size", required = false) Integer size, FeedActivity feedActivity) {
        // Parse request parameters
        Pageable pageable = appService.pageable(page, size, sorting);

        PageWrapper<FeedActivity> list = new PageWrapper<>(feedActivityService.findAll(feedActivity, pageable), "api/feed/list" );

        return ResponseEntity
                .ok(list.getContent());
    }
}
