package io.spring.web.rest;

import io.spring.constant.Resources;
import io.spring.constant.Variables;
import io.spring.exceptionHandler.ResourceNotFoundException;
import io.spring.model.City;
import io.spring.model.Location;
import io.spring.model.Profile;
import io.spring.model.Proposal;
import io.spring.object.Message;
import io.spring.object.ProposalObject;
import io.spring.object.ValidationResponse;
import io.spring.contentWrapper.PageWrapper;
import io.spring.security.TokenHelper;
import io.spring.service.*;
import io.spring.validator.ProposalValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Specifies controller used to handle methods related to
 * <code>ProposalResource</code>
 * (show, list...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@RestController
@RequestMapping("api/")
public class ProposalResource {

    @Autowired
    private AppService appService;

    @Autowired
    private TokenHelper tokenHelper;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private CityService cityService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private ProposalService proposalService;

    @Autowired
    private SearchProposalService searchProposalService;

    @Autowired
    private ProposalValidator proposalValidator;

    /**
     *
     * @param id
     * @return
     */
    @GetMapping({"buy/{id}", "rent/{id}"})
    public ResponseEntity<?> show(@PathVariable Long id) {
        //
        return Optional.ofNullable(proposalService.getById(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new ResourceNotFoundException()
                        .setId(Long.toString(id))
                        .setResourceName(Resources.PROPOSAL));
    }

    /**
     *
     * @param page
     * @param sorting
     * @param proposal
     * @return
     */
    @GetMapping("proposal/list")
    public ResponseEntity<?> list(@RequestParam(value = "page", required = false) Integer page,
                                  @RequestParam(value = "sorting", required = false) String sorting,
                                  @RequestParam(value = "size", required = false) Integer size, Proposal proposal) {
        // Parse request parameters
        Pageable pageable = appService.pageable(page, size, sorting);

        PageWrapper<Proposal> list = new PageWrapper<>(proposalService.findAll(proposal, pageable), "api/proposal/list" );

        return ResponseEntity
                .ok(list);
    }

    /**
     *
     * @param proposalObject
     * @param bindingResult
     * @return
     */
    @PostMapping("buy/new")
    @PreAuthorize("hasRole('ROLE_BUYER')")
    public ResponseEntity<?> createBuying(@RequestBody ProposalObject proposalObject, BindingResult bindingResult, HttpServletRequest request) {
        ValidationResponse res = new ValidationResponse();
        proposalValidator.validate(proposalObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> errorMessages = new ArrayList<>();
            errorMessages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(errorMessages);

            return ResponseEntity.ok(res);
        }

        List<Location> asList = new ArrayList<>();

        Arrays.asList(proposalObject.getLocation().split(",")).forEach(ile -> {
            City city = cityService.getById(ile).get();
            Location location = new Location();
            location.setId(UUID.randomUUID().toString()+appService.getId(Resources.LOCATION));
            location.setCity(city);
            location.setProvince(city.getProvince());
            location.setCountry(city.getProvince().getCountry());
            locationService.save(location);
            asList.add(location);
        });

        String token = tokenHelper.getToken(request);
        Profile profile = profileService.getByCredential(tokenHelper.getUsernameFromToken(token)).get();

        Proposal proposal = new Proposal();
        proposal.setId(appService.getId(Resources.PROPOSAL));
        proposal.setAgeOfProperty(proposalObject.getAgeOfProperty());
        proposal.setStatus(proposalObject.getStatus());
        proposal.setDateOfCreation(appService.getDateTime(Variables.dateTime));
        proposal.setIsUrgent(proposalObject.getUrgent());
        proposal.setBedrooms(proposalObject.getBedrooms());
        proposal.setBathrooms(proposalObject.getBathrooms());
        proposal.setSize(proposalObject.getSize());
        proposal.setPriceMaximum(proposalObject.getPriceMaximum());
        proposal.setPriceMinimum(proposalObject.getPriceMinimum());
        proposal.setEnabled(Boolean.TRUE);
        proposal.setTypeOfProperty(proposalObject.getTypeOfProperty());
        proposal.setTypeOfProposal(proposalObject.getTypeOfProposal());
        proposal.setLocations(asList);
        proposal.setProfile(profile);

        proposalService.save(proposal);

        searchProposalService.asyncSchedule(proposal);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/proposal/new")
                .buildAndExpand("")
                .toUri();

        return ResponseEntity
                .created(location)
                .body(new Message("label.success", "Your buying proposal has been successfully saved", null));

    }

    /**
     *
     * @param proposalObject
     * @param bindingResult
     * @return
     */
    @PostMapping("rent/new")
    @PreAuthorize("hasRole('ROLE_TENANT')")
    public ResponseEntity<?> createRenting(@RequestBody ProposalObject proposalObject, BindingResult bindingResult, HttpServletRequest request) {
        ValidationResponse res = new ValidationResponse();
        proposalValidator.validate(proposalObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> errorMessages = new ArrayList<>();
            errorMessages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(errorMessages);

            return ResponseEntity.ok(res);
        }

        List<Location> asList = new ArrayList<>();

        Arrays.asList(proposalObject.getLocation().split(",")).forEach(ile -> {
            City city = cityService.getById(ile).get();
            Location location = new Location();
            location.setId(UUID.randomUUID().toString()+appService.getId(Resources.LOCATION));
            location.setCity(city);
            location.setProvince(city.getProvince());
            location.setCountry(city.getProvince().getCountry());
            locationService.save(location);
            asList.add(location);
        });

        String token = tokenHelper.getToken(request);
        Profile profile = profileService.getByCredential(tokenHelper.getUsernameFromToken(token)).get();

        Proposal proposal = new Proposal();
        proposal.setId(appService.getId(Resources.PROPOSAL));
        proposal.setAgeOfProperty(proposalObject.getAgeOfProperty());
        proposal.setStatus(proposalObject.getStatus());
        proposal.setDateOfCreation(appService.getDateTime(Variables.dateTime));
        proposal.setIsUrgent(proposalObject.getUrgent());
        proposal.setBedrooms(proposalObject.getBedrooms());
        proposal.setBathrooms(proposalObject.getBathrooms());
        proposal.setSize(proposalObject.getSize());
        proposal.setPriceMaximum(proposalObject.getPriceMaximum());
        proposal.setPriceMinimum(proposalObject.getPriceMinimum());
        proposal.setEnabled(Boolean.TRUE);
        proposal.setTypeOfProperty(proposalObject.getTypeOfProperty());
        proposal.setTypeOfProposal(proposalObject.getTypeOfProposal());
        proposal.setIsFurnished(proposalObject.getIsFurnished());
        proposal.setLocations(asList);
        proposal.setProfile(profile);

        proposalService.save(proposal);

        searchProposalService.asyncSchedule(proposal);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/proposal/new")
                .buildAndExpand("")
                .toUri();

        return ResponseEntity
                .created(location)
                .body(new Message("label.success", "Your renting proposal has been successfully saved", null));

    }

    /**
     *
     * @param proposalObject
     * @param bindingResult
     * @return
     */
    @PutMapping("proposal/update")
    @PreAuthorize("@proposalService.getById(#proposalObject?.id)?.profile?.credential.equals(@authenticationService.getUserAuthenticated().credential)")
    public ResponseEntity<?> update(@RequestBody ProposalObject proposalObject, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        proposalValidator.validate(proposalObject, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> errorMessages = new ArrayList<>();
            errorMessages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(errorMessages);

            return ResponseEntity.ok(res);
        }

        Proposal proposal = proposalService.getById(proposalObject.getId());
        locationService.delete(proposal.getLocations());

        List<Location> asList = new ArrayList<>();

        Arrays.asList(proposalObject.getLocation().split(",")).forEach(ile -> {
            City city = cityService.getById(ile).get();
            Location location = new Location();
            location.setId(UUID.randomUUID().toString()+appService.getId(Resources.LOCATION));
            location.setCity(city);
            location.setProvince(city.getProvince());
            location.setCountry(city.getProvince().getCountry());
            locationService.save(location);
            asList.add(location);
        });

        proposal.setId(proposal.getId());
        proposal.setAgeOfProperty(proposalObject.getAgeOfProperty());
        proposal.setStatus(proposalObject.getStatus());
        proposal.setIsUrgent(proposalObject.getUrgent());
        proposal.setBedrooms(proposalObject.getBedrooms());
        proposal.setBathrooms(proposalObject.getBathrooms());
        proposal.setSize(proposalObject.getSize());
        proposal.setPriceMaximum(proposalObject.getPriceMaximum());
        proposal.setPriceMinimum(proposalObject.getPriceMinimum());
        proposal.setTypeOfProperty(proposalObject.getTypeOfProperty());
        proposal.setTypeOfProposal(proposalObject.getTypeOfProposal());
        proposal.setIsFurnished(proposalObject.getIsFurnished());
        proposal.setLocations(asList);
        proposalService.save(proposal);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body("proposal");
    }

    /**
     *
     * @param id
     * @param proposal
     * @return
     */
    @DeleteMapping("proposal/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id, @RequestBody Proposal proposal) {
        // assertExist(id);
       //  proposalService.save(fileOfProposal);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(proposal);
    }

    /********************************** HELPER METHOD **********************************/
    /*private ResponseEntity<?> assertExist(Long id) {
        return Optional.ofNullable(proposalService.getById(id))
                .map(ResponseEntity::ok)
                .orElseThrow(() -> new ResourceNotFoundException()
                        .setId(Long.toString(id))
                        .setResourceName(Resources.PROPOSAL));
    }*/
}
