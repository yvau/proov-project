package io.spring.web.rest;

import io.spring.constant.Variables;
import io.spring.mail.EmailSender;
import io.spring.model.Profile;
import io.spring.object.CredentialPassword;
import io.spring.object.Message;
import io.spring.object.ValidationResponse;
import io.spring.service.AppService;
import io.spring.service.ProfileInformationService;
import io.spring.service.ProfileService;
import io.spring.validator.RegisterValidator;
import io.spring.validator.ResetPasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Specifies controller used to handle methods related to
 * <code>AuthResource</code>
 * (recover, create...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@RestController
@RequestMapping("oauth/")
public class AuthResource {

    @Autowired
    private AppService appService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private RegisterValidator registerValidator;

    @Autowired
    private ResetPasswordValidator resetPasswordValidator;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private ProfileInformationService profileInformationService;

    @Autowired
    private EmailSender emailSender;

    /**
     *
     * @param credentialPassword
     * @param bindingResult
     * @return
     */
    @PostMapping("password/reset")
    public ResponseEntity<?> reset(@RequestBody CredentialPassword credentialPassword, BindingResult bindingResult) {
        ValidationResponse res = new ValidationResponse();
        resetPasswordValidator.validate(credentialPassword, bindingResult);
        if (bindingResult.hasErrors()) {
            List<FieldError> allErrors = bindingResult.getFieldErrors();
            List<Message> messages = new ArrayList<>();
            messages.addAll(allErrors.stream().map(objectError -> new Message(objectError.getField(), objectError.getCode(), null)).collect(Collectors.toList()));
            res.setMessageList(messages);

            return ResponseEntity.ok(res);
        }

        Profile profile = profileService.getByToken(credentialPassword.getToken()).get();
        profile.setPassword(Variables.PASSWORD_ENCODER.encode(credentialPassword.getPassword()));
        profile.setToken(null);
        profileService.save(profile);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("reset/pwd")
                .buildAndExpand(profile.getId())
                .toUri();

        return ResponseEntity
                .created(location)
                .body("jkj");
    }


    /**
     *
     * @param token
     * @return
     */
    @GetMapping("recover")
    public ResponseEntity<?> recover(@RequestParam(value = "token") String token){
        Optional<Profile> profile = profileService.getByToken(token);
        if(!profile.isPresent())
            return ResponseEntity.ok(new Message("Error !","the token provided is not valid or has expired", null));

        // Return the token
        return ResponseEntity.ok(new Message("Success",token, null));
    }

}
