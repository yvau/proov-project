package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/25/2017
 */
@Controller
public class SellController {

    /**
     * This method handle overview interface
     *
     * @return overview page
     */
    @GetMapping("/sell/")
    public String overview() {
        //
        return "index";
    }

    /**
     * This method handle overview interface
     *
     * @return overview page
     */
    @GetMapping("/sell/new")
    public String create() {
        //
        return "index";
    }
}
