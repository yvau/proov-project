package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Specifies controller used to handle methods related to
 * <code>RentController</code>
 * (show, list...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@Controller
public class RentController {

    /**
     * @return
     */
    @GetMapping("/rent/")
    public String overview() {
        //
        return "index";
    }

    /**
     * @return
     */
    @GetMapping("/rent/new")
    // @PreAuthorize("hasRole('TENANT')")
    public String create() {
        //
        return "index";
    }

    /**
     * @return
     */
    @GetMapping("/rent/{id}")
    public String show(@PathVariable("id") Long id) {
        //
        return "index";
    }

    /**
     * @return
     */
    @GetMapping("/rent/edit/{id}")
    public String edit(@PathVariable("id") Long id) {
        //
        return "index";
    }

    /**
     * @return
     */
    @GetMapping("/rent/list")
    public String list() {
        //
        return "index";
    }
}
