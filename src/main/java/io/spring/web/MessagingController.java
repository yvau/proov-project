package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/30/2017
 */
@Controller
public class MessagingController {

    @GetMapping("/profile/messaging")
    public String list() {
        //
        return "index";
    }
}
