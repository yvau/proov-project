package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 9/19/2017
 */
@Controller
public class PropertyController {

    @GetMapping("/profile/property/new")
    // @PreAuthorize("hasAnyRole('ROLE_SELLER','ROLE_LESSOR')")
    public String create() {
        //
        return "index";
    }

    @GetMapping("/profile/property/edit/{id}")
    // @PreAuthorize("@propertyService.getById(#id)?.profile?.credential.equals(@authenticationService.getUserAuthenticated().credential)")
    public String edit(@PathVariable("id") Long id) {
        //
        return "index";
    }

    @GetMapping("/property/list")
    public String list() {
        //
        return "index";
    }

    @GetMapping("/property/{id}")
    public String show(@PathVariable("id") Long id) {
        System.out.println(id);
        //
        return "index";
    }
}
