package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 9/20/2017
 */
@Controller
public class BuyController {

    /**
     *
     * @return
     */
    @GetMapping("/buy/")
    public String overview() {
        //
        return "index";
    }

    /**
     *
     * @return
     */
    @GetMapping("/buy/new")
    public String create() {
        //
        return "index";
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping("/buy/edit/{id}")
    public String edit(@PathVariable("id") Long id) {
        //
        return "index";
    }

    /**
     *
     * @return
     */
    @GetMapping("/buy/list")
    public String list() {
        //
        return "index";
    }


}
