package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/25/2017
 */
@Controller
public class LeaseController {

    /**
     * @return 'index'
     */
    @GetMapping("/lease/")
    public String overview() {

        return "index";
    }

    /**
     * @return 'index'
     */
    @GetMapping("/profile/lease/new")
    public String create() {

        return "index";
    }
}
