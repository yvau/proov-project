package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

import static io.spring.common.State.populateModel;

/**
 * Specifies controller used to handle methods related to
 * <code>HomeController</code>
 * {home, advantage, howItWorks, relationship, social...}
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 4/14/2017
 */
@Controller
public class HomeController {

    /**
     * This method handle homepage display interface
     *
     * @return home page
     */
    @GetMapping("/")
    public String homePage(Model model, HttpServletRequest request) {
        //
        populateModel(model, request);
        return "index";
    }

    /**
     * This method handle vone-advantage interface
     *
     * @return advantage page
     */
    @GetMapping("/vone-advantage/")
    public String advantage(Model model, HttpServletRequest request) {
        //
        populateModel(model, request);
        return "index";
    }

    /**
     * This method handle vone-concept interface
     *
     * @return concept page
     */
    @GetMapping("/vone-concept/")
    public String concept(Model model, HttpServletRequest request) {
        //
        populateModel(model, request);
        return "index";
    }

    /**
     * @return 'index'
     */
    @GetMapping("/vone-relationship/")
    public String relationship(Model model, HttpServletRequest request) {
        //
        populateModel(model, request);
        return "index";
    }

    /**
     * @return 'index'
     */
    @GetMapping("/social-media-blog/")
    public String social(Model model, HttpServletRequest request) {
        //
        populateModel(model, request);
        return "index";
    }

    /**
     * @return 'index'
     */
    @GetMapping("/about-us/")
    public String aboutUs(Model model, HttpServletRequest request) {
        //
        populateModel(model, request);
        return "index";
    }
}
