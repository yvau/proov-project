package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/30/2017
 */
@Controller
@RequestMapping("profile/payment")
public class PaymentController {

    @GetMapping("/card")
    public String cardPayment() {
        //
        return "index";
    }

    @GetMapping("/paypal")
    public String paypalPayment() {
        //
        return "index";
    }
}
