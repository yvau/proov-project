package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Specifies controller used to handle methods related to
 * <code>Security</code>
 * (login, register, recover, change password...)
 *
 * @author Mobiot Ohouet
 * @version 1.0
 * @date 9/15/2017
 */
@Controller
public class AuthController {

    /**
     * This method handle login display form
     *
     * @return login form to handle the log of user
     * (1) if the user is already logged it redirect to :/profile
     */
    @GetMapping("/login/")
    public String login() {
        /*Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) { // (1)
            return "redirect:/profile/";
        }*/
        return "index";
    }

    /**
     * This method handle registration form
     *
     * @return register form to handle registration
     */
    @GetMapping("/register/")
    public String register() {
        //
        return "index";
    }

    /**
     * @return 'index'
     */
    @GetMapping("/login/recover")
    public String recover() {

        return "index";
    }

    /**
     * @return 'index'
     */
    @GetMapping("/recover/password")
    public String recoverPassword() {

        return "index";
    }
}
