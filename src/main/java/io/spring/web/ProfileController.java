package io.spring.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author Yvau
 * @version 1.0
 * @date 11/25/2017
 */
@Controller
public class ProfileController {

    /**
     * This method handle login display form
     *
     * @return login form to handle the log of user
     * (1) if the user is already logged it redirect to :/profile
     */
    @GetMapping("/profile/edit/role")
    public String editRole() {

        return "index";
    }

    /**
     * This method handle login display form
     *
     * @return login form to handle the log of user
     * (1) if the user is already logged it redirect to :/profile
     */
    @GetMapping("/profile/edit/contact")
    public String editContact() {

        return "index";
    }

    /**
     * This method handle login display form
     *
     * @return login form to handle the log of user
     * (1) if the user is already logged it redirect to :/profile
     */
    @GetMapping("/profile/edit/information")
    public String editInformation() {

        return "index";
    }

    /**
     * This method handle login display form
     *
     * @return login form to handle the log of user
     * (1) if the user is already logged it redirect to :/profile
     */
    @GetMapping("/profile/edit/password")
    public String editPassword() {

        return "index";
    }
}
