package io.spring.security;

import io.spring.model.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yvau
 * @version 1.0
 * @date 12/14/2017
 */
public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(Profile profile) {
        return new JwtUser(
                profile.getId(),
                profile.getCredential(),
                profile.getPassword(),
                profile.getEnabled(),
                mapToGrantedAuthorities(profile.getRole()),
                profile.getDateOfCreation()
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(String role) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRolesAsList(role));
        return authList;
    }

    /**
     * @param roles
     * @return
     */
    private static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

    /**
     *
     * @param role
     * @return
     */
    private static List<String> getRolesAsList(String role) {
        List<String> rolesAsList = new ArrayList<>();
        String[] arrayRole = role.split(",");
        for(String roles : arrayRole){
            rolesAsList.add(roles);
        }
        return rolesAsList;
    }
}
