'use strict'
require('./check-versions')()

process.env.NODE_ENV = 'production'

const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const shell = require('shelljs')
const config = require('../config')

const spinner = ora('building server for production...')
spinner.start()

rm(path.join(config.build.assetsServerRoot), err => {
  if (err) throw err
  shell.exec('gulp', function(code, stdout, stderr) {
    spinner.stop()
    if (stderr) throw stderr

    shell.sed('-i', 'BUILD_MANIVEST', shell.cat('../src/main/resources/static/json/asset-manifest.json'), 'dist/index.js');
    shell.cp('-R', 'dist/', '../src/main/resources/render/')
    shell.cp('-R', 'src/utils/_*.js', '../src/main/resources/render/')
    shell.cp('-R', 'node_modules/vue-server-renderer/basic.js', '../src/main/resources/render/')
    shell.cp('-R', 'node_modules/vue/dist/vue.js', '../src/main/resources/render/')
    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow('  Server chunk file ready.\n'))
  })
  /* 

    shell.sed('-i', 'BUILD_MANIVEST', shell.cat('../src/main/resources/static/json/asset-manifest.json'), '../src/main/resources/server-bundle.js');
    shell.cat(['../src/main/resources/server-bundle.js', './src/return-render-function.js']).to('../src/main/resources/render/index.js')
  }) */
})
