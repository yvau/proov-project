'use strict'
require('./check-versions')()

process.env.NODE_ENV = 'production'

const ora = require('ora')
const rm = require('rimraf')
const path = require('path')
const chalk = require('chalk')
const webpack = require('webpack')
const shell = require('shelljs')
const config = require('../config')
const webpackConfig = require('./webpack.server.conf')

const spinner = ora('building server for production...')
spinner.start()


  webpack(webpackConfig, function (err, stats) {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    shell.sed('-i', 'BUILD_MANIVEST', shell.cat('../src/main/resources/static/json/asset-manifest.json'), '../src/main/resources/server-bundle.js');
    shell.cat(['../src/main/resources/server-bundle.js', './src/return-render-function.js']).to('../src/main/resources/render/index.js')

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      process.exit(1)
    }

    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow('  Server chunk file ready.\n'))
  })

