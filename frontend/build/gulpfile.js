var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var babelify = require('babelify');
var babel = require('gulp-babel');
var vueify = require('vueify');
var envify = require('envify');
var gutil = require('gulp-util');
var path = require('path');

//   .pipe(exec('cat ./dist/build/server-bundle.js ../src/main/resources/render/index.js > ../src/main/resources/render/essaindex.js'))

gulp.task('default', () => {
  browserify({
    entries: 'src/entry-server.js',
    debug: false,
    standalone: 'server',
    extensions: ['.js', '.vue', '.json'],
    paths: ['./node_modules','./src/'],
    // defining transforms here will avoid crashing your stream
	transform: [[babelify], [vueify], [envify, {'global': true, NODE_ENV: 'production'}]]
  })
  .bundle()
  .on('error', err => {
    gutil.log("Browserify Error", gutil.colors.red(err.message))
  })
  .pipe(source('server-bundle.js'))
  .pipe(gulp.dest('./dist/build'))
});
