var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var babelify = require('babelify');
var vueify = require('vueify');
var envify = require('envify');
var gutil = require('gulp-util');

gulp.task('default', () => {
  browserify({
    entries: 'src/entry-server.js',
    debug: false,
    standalone: 'server',
    extensions: ['.js', '.vue', '.json'],
    paths: ['./node_modules','./src/'],
    // defining transforms here will avoid crashing your stream
	transform: [[babelify], [vueify], [envify, {'global': true, NODE_ENV: 'production'}]]
  })
  .bundle()
  .on('error', err => {
    gutil.log("Browserify Error", gutil.colors.red(err.message))
  })
  .pipe(source('index.js'))
  .pipe(gulp.dest('./dist'))
});