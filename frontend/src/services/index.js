import errorMessage from './errorMessage'
import removeMessage from './removeMessage'
import checkImage from './checkImage'

export default {
  errorMessage,
  removeMessage,
  checkImage
}
