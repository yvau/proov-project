/* ============
 * Account Transformer
 * ============
 *
 * The transformer for the account.
 */

import Transformer from './Transformer'
import service from 'services'
import store from 'store'

export default class AccountTransformer extends Transformer {
  /**
   * Method used to transform a fetched account.
   *
   * @param account The fetched account.
   *
   * @returns {Object} The transformed account.
   */
  static fetch (account) {
    return {
      id: account.id,
      credential: account.credential,
      firstName: account.profileInformation.firstName,
      lastName: account.profileInformation.lastName,
      gender: account.profileInformation.gender,
      birthDate: account.profileInformation.birthDate,
      bestWayToReachYou: account.profileInformation.bestWayToReachYou,
      homePhone: account.profileInformation.homePhone,
      officePhone: account.profileInformation.officePhone,
      emailContact: account.profileInformation.emailContact,
      role: account.role
    }
  }

  /**
   * Method used to transform a send role.
   *
   * @param role The role to be send.
   *
   * @returns {Object} The transformed role.
   */
  static send (account) {
    let formData = new FormData()
    let images = store.state.global.object
    if (service.checkImage(images)) {
      formData.append('photo', images.file)
    }
    formData.append('firstName', account.firstName)
    formData.append('lastName', account.lastName)
    formData.append('gender', account.gender)
    formData.append('birthDate', account.birthDate)
    return formData
  }
}
