import Proxy from './Proxy'

class PropertyProxy extends Proxy {
  /**
   * The constructor for the PropertyProxy.
   *
   * @param {Object} parameters The query parameters.
   */
  constructor (parameters = {}) {
    super('/api/property', parameters)
  }

}

export default PropertyProxy
