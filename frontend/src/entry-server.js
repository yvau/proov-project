import awaitServer from './awaitServer'
import App from './App'
/* eslint-disable no-undef */

global.renderView = function (_viewName, model, url) {
  let assetManifest = BUILD_MANIVEST
  let cssPath = 'static/css/main.css'
  let manifest = 'static/js/manifest.js'
  let vendor = 'static/js/vendor.js'
  let app = 'static/js/app.js'

  if (assetManifest) {
    manifest = assetManifest['manifest.js']
    vendor = assetManifest['vendor.js']
    app = assetManifest['app.js']
    cssPath = assetManifest['app.css']
  }

  function populateTemplate (markup) {
    return `<!doctype html>
    <html>
     <head>
       <link rel='stylesheet' href='/${cssPath}' />
       <meta charset='utf-8' />
       <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no' />
       <script src='https://www.google.com/recaptcha/api.js' async defer></script>
      </head>
  <body class='fixed-header horizontal-menu horizontal-app-menu dashboard'>
    ${markup}
   <script>(function(d, s, id) {
     var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.11&appId=552225261785388';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
   <script src="https://apis.google.com/js/api:client.js"></script>
   <script type='text/javascript' src='/${manifest}'></script>
   <script type='text/javascript' src='/${vendor}'></script>
   <script type='text/javascript' src='/${app}'></script>
   <script src='https://js.stripe.com/v3/'></script>
 </body>
</html>`
  }

  const serializer = (new Packages.com.fasterxml.jackson.databind.ObjectMapper()).writer()

  function getData(model) {
      const renderData = { data: {} }
      for (let key in model) {
        if (key.startsWith('__')) {
          renderData[ key.substring(2) ] = model[key]
        }
        else {
          renderData.data[key] = model[key]
        }
      }

      /* Serialise the model for passing to the client. We don't use JSON.stringify
       * because Nashorn's version doesn't cope with POJOs by design.
       *
       * http://www.slideshare.net/SpringCentral/serverside-javascript-with-nashorn-and-spring
       */
      renderData.json = serializer.writeValueAsString(renderData.data)

      /* "Purify" the model by swapping it for the serialised version */
      renderData.data = JSON.parse(renderData.json)

      return renderData
  }

   const { requestPath, data, json } = getData(model)

   console.log(requestPath)
   console.log(data)
   console.log(json)

  let vm = new Vue({
    template: '<App/>',
    components: { App }
  })

  let inWait = awaitServer(function (done) {
    renderVueComponentToString(vm, function (err, res) {
      done(err, res)
    })
  })

  if (inWait.error) {
    console.log(inWait.error)
  } else {
    return populateTemplate(inWait.result)
  }
}
