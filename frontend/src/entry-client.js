import { createApp } from 'main'
import 'plugins/vuex'
import 'plugins/vuex-router-sync'
import 'plugins/axios'
import 'plugins/icons'
import 'plugins/pace'
import 'plugins/multiselect'
import 'plugins/validator'
import 'plugins/jquery'
// import 'plugins/vueAgile'
// import 'plugins/facebook-button'
// import 'plugins/google-button'
import 'plugins/vue-breadcrumbs'
// import 'plugins/semantic-ui'
import 'plugins/stylesheet'
// client-specific bootstrapping logic...

const { app } = createApp()

// this assumes App.vue template root element has `id="app"`
app.$mount('#app')
