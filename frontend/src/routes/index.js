/* ============
 * Routes File
 * ============
 *
 * The routes and redirects are defined in this file.
 */

export default [

  // Home
  {
    path: '/',
    component: () => import('pages/Index/Index'),

    // Any user can see this page
    meta: {
      breadcrumb: 'Home'
    },
    children: [
      // About us
      {
        path: '',
        name: 'home',
        component: () => import('pages/Home/Index'),

        // Any user can see this page
        meta: {
          guest: true
        }
      },
      // About us
      {
        path: '/about-us/',
        name: 'aboutus',
        component: () => import('pages/About/Index'),

        // Any user can see this page
        meta: {
          guest: true,
          breadcrumb: 'About us'
        }
      },
      // Concept
      {
        path: '/vone-concept/',
        name: 'voneconcept',
        component: () => import('pages/Concept/Index'),

        // Any user can see this page
        meta: {
          guest: true,
          breadcrumb: 'Vone concept'
        }
      },
      // Advantage
      {
        path: '/vone-advantage/',
        name: 'voneadvantage',
        component: () => import('pages/Advantage/Index'),

        // Any user can see this page
        meta: {
          guest: true,
          breadcrumb: 'Vone advantage'
        }
      },
      // Relationship
      {
        path: '/vone-relationship/',
        name: 'vonerelationship',
        component: () => import('pages/Relationship/Index'),

        // Any user can see this page
        meta: {
          guest: true,
          breadcrumb: 'Vone relationship'
        }
      },
      // Social Blog
      {
        path: '/social-media-blog/',
        component: () => import('pages/SocialAndBlog/Index'),

        // If the user needs to be authenticated to view this page
        meta: {
          breadcrumb: 'Social media blog'
        },
        children: [
          // Blog Show
          {
            path: '',
            name: 'socialmedia',
            component: () => import('pages/SocialAndBlog/_Index/Index'),

            // If the user needs to be authenticated to view this page
            meta: {
              guest: true
            }
          },
          // Blog Show
          {
            path: '/blog/:id(\\d+)',
            name: 'socialmedia.show',
            component: () => import('pages/SocialAndBlog/Show/Index'),

            // If the user needs to be authenticated to view this page
            meta: {
              guest: true,
              breadcrumb: 'Show'
            }
          }
        ]
      },
      // Term of use
      {
        path: '/term-of-use/',
        name: 'termofuse',
        component: () => import('pages/TermOfUse/Index'),

        // If the user needs to be authenticated to view this page
        meta: {
          guest: true,
          breadcrumb: 'Term of use'
        }
      },
      // Privacy of policy
      {
        path: '/privacy-policy/',
        name: 'privacypolicy',
        component: () => import('pages/TermOfUse/Index'),

        // If the user needs to be authenticated to view this page
        meta: {
          guest: true,
          breadcrumb: 'Privacy policy'
        }
      },
      // Login
      {
        path: '/login/',
        name: 'login',
        component: () => import('pages/Login/Index'),

        // If the user needs to be a guest to view this page.
        meta: {
          guest: true,
          breadcrumb: 'Login'
        }
      },
      // Register
      {
        path: '/register/',
        name: 'register',
        component: () => import('pages/Register/Index.vue'),

        // If the user needs to be a guest to view this page.
        meta: {
          guest: true,
          breadcrumb: 'Register'
        }
      },
      // Recover form
      {
        path: '/login/recover',
        name: 'recoverForm.index',
        component: () => import('pages/Recover/Index.vue'),

        // If the user needs to be a guest to view this page.
        meta: {
          guest: true,
          breadcrumb: 'Recover'
        }
      },
      // Recover password
      {
        path: '/recover/password',
        name: 'recoverPassword.index',
        component: () => import('pages/Recover/Password/Index.vue'),

        // If the user needs to be a guest to view this page.
        meta: {
          guest: true,
          breadcrumb: 'Recover password'
        }
      },
      // Activate account
      {
        path: '/activate',
        name: 'activate.index',
        component: () => import('pages/Activate/Index.vue'),

        // If the user needs to be a guest to view this page.
        meta: {
          guest: true,
          breadcrumb: 'Activate'
        }
      },
      // Feeds
      {
        path: '/feeds/',
        name: 'feeds',
        component: () => import('pages/Feeds/Index'),

        // If the user needs to be authenticated to view this page
        meta: {
          guest: true,
          breadcrumb: 'Feed'
        }
      },
      // Buy
      {
        path: '/buy/',
        component: () => import('pages/Buy/Index.vue'),
        // If the user needs to be a guest to view this page.
        meta: {
          guest: true,
          breadcrumb: 'Buying proposal'
        },
        children: [
          // Buying list
          {
            path: 'list',
            name: 'buy.list',
            component: () => import('pages/Buy/_List/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'List'
            }
          },
          // Profile new rent
          {
            path: 'new',
            name: 'buy.new',
            component: () => import('pages/Buy/_New/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'New'
            }
          },
          // Profile edit buy
          {
            path: 'edit/:id(\\d+)',
            name: 'buy.edit',
            component: () => import('pages/Buy/_Edit/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'Edit'
            }
          },
          {
            path: 'overview',
            name: 'buy.overview',
            component: () => import('pages/Buy/_Overview/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'Overview'
            }
          },
          {
            path: ':id(\\d+)',
            component: () => import('pages/Buy/_Show/Index.vue'),
            meta: {
              guest: true,
              breadcrumb: 'Show'
            },
            children: [
              { path: '', name: 'buy.show.overview', component: () => import('pages/Buy/_Show/_Overview/Index.vue'), meta: { guest: true } },
              { path: 'statistics', name: 'buy.show.statistics', component: () => import('pages/Buy/_Show/_Statistics/Index.vue'), meta: { guest: true, breadcrumb: 'Statistics' } },
              { path: 'proposition', name: 'buy.show.propositions', component: () => import('pages/Buy/_Show/_Propositions/Index.vue'), meta: { guest: true, breadcrumb: 'Propositions' } },
              { path: 'settings', name: 'buy.show.settings', component: () => import('pages/Buy/_Show/_Settings/Index.vue'), meta: { guest: true, breadcrumb: 'Settings' } }
            ]
          },
          {
            path: '',
            name: 'buy.index',
            component: () => import('pages/Buy/_Index/Index.vue')
            // If the user needs to be a guest to view this page.
            /* meta: {
              guest: true,
              breadcrumb: 'buying proposal overview'
            } */
          }
        ]
      },
      // Rent
      {
        path: '/rent/',
        component: () => import('pages/Rent/Index.vue'),
        // If the user needs to be a guest to view this page.
        meta: {
          guest: true,
          breadcrumb: 'Renting proposal'
        },
        children: [
          {
            path: 'list',
            name: 'rent.list',
            component: () => import('pages/Rent/_List/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'List'
            }
          },
          // Profile new rent
          {
            path: 'new',
            name: 'rent.new',
            component: () => import('pages/Rent/_New/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'New'
            }
          },
          // Profile edit buy
          {
            path: 'edit/:id(\\d+)',
            name: 'rent.edit',
            component: () => import('pages/Rent/_Edit/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'Edit'
            }
          },
          {
            path: 'overview',
            name: 'rent.overview',
            component: () => import('pages/Rent/_Overview/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'Overview'
            }
          },
          {
            path: ':id(\\d+)',
            component: () => import('pages/Rent/_Show/Index.vue'),
            meta: {
              guest: true,
              breadcrumb: 'Show'
            },
            children: [
              { path: '', name: 'rent.show.overview', component: () => import('pages/Rent/_Show/_Overview/Index.vue'), meta: { guest: true, breadcrumb: 'Statistics' } },
              { path: 'statistics', name: 'rent.show.statistics', component: () => import('pages/Rent/_Show/_Statistics/Index.vue'), meta: { guest: true, breadcrumb: 'Statistics' } },
              { path: 'proposition', name: 'rent.show.propositions', component: () => import('pages/Rent/_Show/_Propositions/Index.vue'), meta: { guest: true, breadcrumb: 'Propositions' } },
              { path: 'settings', name: 'rent.show.settings', component: () => import('pages/Rent/_Show/_Settings/Index.vue'), meta: { guest: true, breadcrumb: 'Settings' } }
            ]
          },
          {
            path: '',
            name: 'rent.index',
            component: () => import('pages/Rent/_Index/Index.vue')
            // If the user needs to be a guest to view this page.
            /* meta: {
              guest: true,
              breadcrumb: 'buying proposal overview'
            } */
          }
        ]
      },
      // Property
      {
        path: '/property/',
        component: () => import('pages/Property/Index.vue'),
        // If the user needs to be a guest to view this page.
        meta: {
          guest: true,
          breadcrumb: 'Property'
        },
        children: [
          {
            path: 'list',
            name: 'property.list',
            component: () => import('pages/Property/_List/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'List'
            }
          },
          // Profile new rent
          {
            path: 'new',
            name: 'property.new',
            component: () => import('pages/Property/_New/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'New'
            }
          },
          // Profile edit buy
          {
            path: 'edit/:id(\\d+)',
            name: 'property.edit',
            component: () => import('pages/Property/_Edit/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'Edit'
            }
          },
          {
            path: ':id(\\d+)',
            component: () => import('pages/Property/_Show/Index.vue'),
            meta: {
              guest: true,
              breadcrumb: 'Show'
            },
            children: [
              { path: '', name: 'property.show.overview', component: () => import('pages/Property/_Show/_Overview/Index.vue'), meta: { guest: true, breadcrumb: 'Statistics' } },
              { path: 'statistics', name: 'property.show.statistics', component: () => import('pages/Property/_Show/_Statistics/Index.vue'), meta: { guest: true, breadcrumb: 'Statistics' } },
              { path: 'proposition', name: 'property.show.propositions', component: () => import('pages/Property/_Show/_Propositions/Index.vue'), meta: { guest: true, breadcrumb: 'Propositions' } },
              { path: 'settings', name: 'property.show.settings', component: () => import('pages/Property/_Show/_Settings/Index.vue'), meta: { guest: true, breadcrumb: 'Settings' } }
            ]
          }
        ]
      },
      // Lease
      {
        path: '/lease/',
        name: 'lease.index',
        component: () => import('pages/Lease/Index.vue'),

        // If the user needs to be a guest to view this page.
        meta: {
          guest: true,
          breadcrumb: 'Lease'
        },
        children: [
          { path: 'overview', name: 'lease.overview', component: () => import('pages/Lease/_Overview/Index.vue'), meta: { guest: true, breadcrumb: 'overview' } },
          { path: 'new', name: 'lease.new', component: () => import('pages/Lease/_New/Index.vue'), meta: { guest: true, breadcrumb: 'new leasing' } }
        ]
      },
      // Profile new rent
      {
        path: '/profile/',
        component: () => import('pages/Profile/Index.vue'),
        // If the user needs to be a guest to view this page.
        meta: {
          guest: true,
          breadcrumb: 'profile'
        },
        children: [
          // Profile get_information
          {
            path: '/profile/get_information',
            name: 'profile.getinformation',
            component: () => import('pages/Profile/Get/Information/Index.vue'),

            // If the user needs to be a guest to view this page.
            meta: {
              guest: true,
              breadcrumb: 'Get information'
            }
          },
          {
            path: 'edit',
            meta: {
              breadcrumb: 'edit'
            },
            component: () => import('pages/Profile/_Edit/Index.vue'),
            children: [
              { path: '', name: 'profile.edit', component: () => import('pages/Profile/_Edit/_Index/Index.vue'), meta: { guest: true } },
              { path: 'password', name: 'profile.edit.password', component: () => import('pages/Profile/_Edit/_Password/Index.vue'), meta: { guest: true, breadcrumb: 'Password' } },
              { path: 'contact', name: 'profile.edit.contact', component: () => import('pages/Profile/_Edit/_Contact/Index.vue'), meta: { guest: true, breadcrumb: 'Contact' } },
              { path: 'role', name: 'profile.edit.role', component: () => import('pages/Profile/_Edit/_Role/Index.vue'), meta: { guest: true, breadcrumb: 'Role' } },
              { path: 'information', name: 'profile.edit.information', component: () => import('pages/Profile/_Edit/_Information/Index.vue'), meta: { guest: true, breadcrumb: 'Information' } }
            ]
          },
          {
            path: '',
            name: 'profile.show',
            component: () => import('pages/Profile/_Index/Index.vue')
            // If the user needs to be a guest to view this page.
            /* meta: {
              guest: true,
              breadcrumb: 'buying proposal overview'
            } */
          }
        ]
      }
    ]
  },

  // Bookmarks
  {
    path: '/bookmarks/',
    name: 'bookmarks.index',
    component: () => import('pages/Bookmarks/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      guest: true,
      breadcrumb: 'Bookmarks'
    }
  },

  // Notifications
  {
    path: '/notifications/',
    name: 'notification.index',
    component: () => import('pages/Feeds/Index'),

    // If the user needs to be authenticated to view this page
    meta: {
      guest: true,
      breadcrumb: 'Notifications'
    }
  },

  // Account
  {
    path: '/account/',
    name: 'account.index',
    component: () => import('pages/Account/Index'),

    // If the user needs to be authenticated to view this page.
    meta: {
      guest: true,
      breadcrumb: 'Account'
    }
  },

  // Account Show
  {
    path: '/account/:id(\\d+)',
    name: 'account.showx',
    component: () => import('pages/Account/Show/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true,
      breadcrumb: 'Show'
    }
  },

  // Account List
  {
    path: '/account/list',
    name: 'accountList.index',
    component: () => import('pages/Account/List/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true,
      breadcrumb: 'Bar Page'
    }
  },

  // Sell
  {
    path: '/sell/',
    name: 'sell.index',
    component: () => import('pages/Sell/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true,
      breadcrumb: 'Sell'
    }
  },

  // Profile new property
  {
    path: '/sell/new',
    name: 'sell.new',
    component: () => import('pages/Profile/Sell/New/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true,
      breadcrumb: 'Selling form'
    }
  },

  // Profile edit information
  {
    path: '/payment/paypal',
    name: 'paymentPaypal.index',
    component: () => import('pages/Payment/Paypal/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true,
      breadcrumb: 'payment paypal'
    }
  },

  // Profile edit information
  {
    path: '/payment/card',
    name: 'paymentCard.index',
    component: () => import('pages/Payment/Card/Index.vue'),

    // If the user needs to be a guest to view this page.
    meta: {
      guest: true,
      breadcrumb: 'Card'
    }
  },

  /* {
    path: '/',
    redirect: '/home'
  }, */

  {
    path: '/*',
    redirect: '/'
  }
]
