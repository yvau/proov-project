import getFurnished from './getFurnished'
import setFurnished from './setFurnished'
import setUrgent from './setUrgent'
import getUrgent from './getUrgent'

export default {
  getFurnished,
  setFurnished,
  getUrgent,
  setUrgent
}
