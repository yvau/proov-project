/* ============
 * Mutations for the auth module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

import Axios from 'axios'
import {
  CHECK,
  LOGIN,
  LOGOUT
} from './mutation-types'

export default {
  [CHECK] (state) {
    state.authenticated = !!localStorage.getItem('AUTH-ID')
    if (state.authenticated) {
      Axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('AUTH-ID')}`
    }
  },

  [LOGIN] (state, payload) {
    state.authenticated = true
    localStorage.setItem('AUTH-ID', payload.access_token)
    Axios.defaults.headers.common['Authorization'] = `Bearer ${payload.access_token}`
  },

  [LOGOUT] (state) {
    state.authenticated = false
    localStorage.removeItem('AUTH-ID')
    Axios.defaults.headers.common['Authorization'] = ''
  }
}
