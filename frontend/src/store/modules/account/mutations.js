/* ============
 * Mutations for the account module
 * ============
 *
 * The mutations that are available on the
 * account module.
 */

// import Vue from 'vue'
import { FIND, RELOGIN } from './mutation-types'
// import cookies from 'browser-cookies'

export default {
  [FIND] (state, payload) {
    console.log(payload)
    state.id = payload.id
    state.credential = payload.credential
    state.firstName = payload.profileInformation.firstName
    state.lastName = payload.profileInformation.lastName
    state.gender = payload.profileInformation.gender
    state.birthDate = payload.profileInformation.birthDate
    state.role = payload.role.split(',')
    state.bestWayToReachYou = payload.profileInformation.bestWayToReachYou
    state.emailContact = payload.profileInformation.emailContact
    state.officePhone = payload.profileInformation.officePhone
    state.homePhone = payload.profileInformation.homePhone
    state.photo = payload.profileInformation.profilePhoto.url
  },

  [RELOGIN] (state, payload) {
    // cookies.set('AUTH-TOKEN-X', payload.token, {expires: 600})
    state.authenticated = true
    // Vue.$http.defaults.headers.common.Authorization = `Bearer ${payload.token}`
  }
}
