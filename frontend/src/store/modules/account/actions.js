/* ============
 * Actions for the account module
 * ============
 *
 * The actions that are available on the
 * account module.
 */

// import store from 'store'
// import RoleTransformer from 'transformers/RoleTransformer'
// import service from 'services'
import * as types from './mutation-types'
import Proxy from 'proxies/AccountProxy'

export const find = ({ commit }, payload) => {
  new Proxy()
    .find('get')
    .then((response) => {
      commit(types.FIND, response)
    })
    .catch(() => {
      console.log('Request failed...')
    })
}

/* export const changeRole = ({ commit }, payload) => {
  new Proxy()
    .changeRole(RoleTransformer.send(payload))
    .then((response) => {
      try {
        service.errorMessage(response)
      } catch (err) {
        response.status = true
        commit(types.RELOGIN, response.object)
        store.dispatch('message/flashMessage', response)
        store.dispatch('account/find')
      }
    })
    .catch(() => {
      console.log('Request failed...')
    })
} */

export default {
  find
}
