/* ============
 * Actions for the auth module
 * ============
 *
 * The actions that are available on the
 * auth module.
 */

import * as types from './mutation-types'

export const flashMessage = ({ commit }, payload) => {
  commit(types.FLASHMESSAGE, payload)
}

export default {
  flashMessage
}
