/* ============
 * Vue Validate
 * ============
 *
 * Validate
 *
 * https://github.com/semisleep/simple-vue-validator
 */
import Vue from 'vue'

var SimpleVueValidation = require('simple-vue-validator')
Vue.use(SimpleVueValidation)
